package ua.hillel.oop.encapsulation;

public class Car {

    private void startElectricity() {
        System.out.println("Start Electricity");
    }

    private void startCommand() {
        System.out.println("Start Command");
    }

    private void startFuelSystem() {
        System.out.println("Start FuelSystem");
    }

    public void start() {
        startElectricity();
        startCommand();
        startFuelSystem();
    }
}
