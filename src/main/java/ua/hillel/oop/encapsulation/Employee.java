package ua.hillel.oop.encapsulation;

public class Employee {
    private final String fullName;
    private final String position;
    private final String email;
    private final String phone;
    private final int age;

    public Employee(String fullName, String position, String email, String phone, int age) {
        this.fullName = fullName;
        this.position = position;
        this.email = email;
        this.phone = phone;
        this.age = age;
    }

    public Employee(String fullName, int age) {
        this.fullName = fullName;
        this.position = "Worker";
        this.email = "n/a";
        this.phone = "n/a";
        this.age = age;
    }

    @Override
    public String toString () {
        String s = "FullName: " + this.fullName + ", position: " + this.position + ", e-mail: " + this.email +
                   ", phone: " + this.phone + ", age: " + this.age;
        return s;
    }
}
