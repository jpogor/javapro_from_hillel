package ua.hillel.oop.encapsulation;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Max", "Software developer", "bigmax@gmail.com", "067-480-4920", 25);
        Employee employee2 = new Employee("Max", 31);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println();

        SameName sameName = new SameName("SameName");
        System.out.println(sameName);  // "адрес" экземпляра класса
        System.out.println();

        Car car = new Car();
        System.out.println("Start Car");
        car.start();
    }
}
