package ua.hillel.oop.encapsulation;

public class SameName {
    private final String name;

    public SameName(String name) {
        this.name = name;
    }
}
