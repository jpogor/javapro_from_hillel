package ua.hillel.oop.hw06strings;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int count = MyString.findSymbolOccurance ("Thkjbа\nfsаbcb\nоyb69а98u0аbhy_joihiugy", '\n');
        System.out.println("Count of symbol = " + count);

        int pos = MyString.findWordPosition("Apollo", "pollo");
        System.out.println("findWordPosition = " + pos);
        pos = MyString.findWordPosition("Apple", "Ple");
        System.out.println("findWordPosition = " + pos);

        System.out.println(MyString.stringReverse("Hello")); // Hello -> olleH
        System.out.println(MyString.stringReverse(MyString.stringReverse("Hello"))); // Hello -> olleH
        System.out.println(MyString.stringReverse("Дракоша"));
        System.out.println(MyString.stringReverse(MyString.stringReverse("Дракоша")));

        System.out.println(MyString.isPalindrome("ERE")); // true
        System.out.println(MyString.isPalindrome("Allo")); // false

        MyGame();

    }

    private static void MyGame() {
        String[] words = {
                "apple", "orange", "lemon", "banana", "apricot", "avocado" , "broccoli", "carrot", "cherry", "garlic",
                "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", " pea", "peanut", "pear",
                "pepper", "pineapple", "pumpkin", "potato"
        };

        int rnd = (int) (Math.random() * words.length);
        String riddle = words[rnd];
        System.out.println("Загадано = " + riddle); // для упрощения ручного тестирования
        String answer = "";

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово:");
        while (sc.hasNext()) {
            answer = sc.next();
            System.out.println("Ваш ответ: " + answer);
            if (answer.equals("quit"))
                break;
            answer = MyString.compareWords(answer, riddle);
            if (answer.equals(riddle)) {
                System.out.println("Вы угадали !!!");
                break;
            } else {
                System.out.println("Подсказка: " + answer + ". Попробуйте еще:");
            }
        }
    }
}
