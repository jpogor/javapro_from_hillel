package ua.hillel.oop.hw06strings;

public class MyString {
    public static int findSymbolOccurance(String s, Character ch) {
        int cnt = 0;
        for (int i = 0; i < s.length(); i++) {
            if (ch.equals(s.charAt(i)))
                cnt++;
        }
        return cnt;
    }

    public static int findWordPosition(String source, String target) {
        //if (!source.contains(target))   return -1;
        int pos = source.indexOf(target);
        return pos;
    }

    public static String stringReverse(String s) {
        String res = "";
        char[] c = s.toCharArray();
        for (int i = c.length-1; i >= 0; i--) {
            res += c[i];
        }
        return res;
    }

    public static boolean isPalindrome(String s) {
        String s1 = stringReverse(s);
        return s1.equals(s);
    }

    public static String compareWords(String answer, String riddle) {
        String hint = "";
        answer = answer.toLowerCase();
        riddle = riddle.toLowerCase();
        int maxlen = Math.max(answer.length(),riddle.length());
        for (int i = 0; i < maxlen; i++) {
            char a = (i < answer.length()) ? answer.charAt(i) : '*';
            char r = (i < riddle.length()) ? riddle.charAt(i) : '*';
            if (a != r) {
                a = '*';
            }
            hint += a;
        }
        if (!hint.equals(riddle))
            for (int i = hint.length(); i < 15; i++)
                hint += "*";
        return hint;
    }
}
