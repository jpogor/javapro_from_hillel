package ua.hillel.oop.hw07exception;

public class ArrayDataException extends RuntimeException {
    public ArrayDataException(String message) {
        super(message);
    }
    public ArrayDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
