package ua.hillel.oop.hw07exception;

public class ArrayValueCalculator {

    public int doCalc(String[][] sArr) throws ArraySizeException, ArrayDataException {
        int sum = 0;
        if (sArr.length != 4)
            throw new ArraySizeException("Error: Wrong size of Array. It must be 4x4.");
        for (int i = 0; i < 4; i++) {
            if (sArr[i].length != 4)
                throw new ArraySizeException("Error: Wrong size of Array. Line " + (i+1) + " must have size = 4. Actual size = " + sArr[i].length);
            for (int j = 0; j < 4; j++) {
                try {
                    sum += Integer.parseInt(sArr[i][j]);
                } catch (RuntimeException e) {
                    throw new ArrayDataException("Error: Can't parse String to int: Arr[" + (i+1) + "][" + (j+1) + "] = " + sArr[i][j], e);
                }
            }
        }
        return sum;
    }
}

