package ua.hillel.oop.hw07exception;

public class Main {
    public static void main(String[] args) {
        String[][] sArr = {
                {"1","2","3","4"},
                {"1","2","3","4"},
                {"1","2","3","4"},
                {"1","2","3","5"}
        };
        //ArrayValueCalculator arrayValueCalculator = new ArrayValueCalculator();
        var arrayValueCalculator = new ArrayValueCalculator();
        try {
            int total = arrayValueCalculator.doCalc(sArr);
            System.out.println("Total = " + total);
        } catch (ArrayDataException | ArraySizeException e) {
            System.out.println(e);
            throw new RuntimeException(e);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }
}
