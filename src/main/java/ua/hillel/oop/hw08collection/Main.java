package ua.hillel.oop.hw08collection;

import ua.hillel.oop.hw08collection.phonebook.PhoneBook;
import ua.hillel.oop.hw08collection.phonebook.PhoneRecord;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Task 1 check
        System.out.println("1. " + "=".repeat(80));
        List<String> srcWords = List.of("apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli",
                "carrot", "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato");
        var words = new ArrayList<String>();
        for(int i = 0; i < 100; i++) {
            int randomIndex = (int) (Math.random() * srcWords.size());
            words.add(srcWords.get(randomIndex));
        }
        int cnt = MyCollection.countOccurance(words, "grape");
        System.out.println("countOccurance() = " + cnt);

        // Task 2 check
        System.out.println("2. " + "=".repeat(80));
        String[] arr = {"1","2","3","4","5","6","7","8","9","10","11","12"};
        var resList = MyCollection.toList(arr);
        System.out.println("toList() = " + resList);

        // Task 3 check
        System.out.println("3. " + "=".repeat(80));
        var srcList = new ArrayList<String>();
        srcList.addAll(resList);
        resList = MyCollection.findUnique(srcList);
        System.out.println("findUnique() = " + resList);

        srcList.clear();
        for(int i = 0; i < 100; i++) {
            int randomInt = (int) (Math.random() * 100) + 1;
            srcList.add(String.valueOf(randomInt));
        }
        resList = MyCollection.findUnique(srcList);
        System.out.println("findUnique() = " + resList);

        // Task 4 check
        System.out.println("4. " + "=".repeat(80));
        List<MyCollection.StringCounter> res = MyCollection.findOccurance(List.of("bird","bird","fox","cat","fox"));
        System.out.println("findOccurance() = " + res);
        res.clear();
        res = MyCollection.findOccurance(srcWords);
        System.out.println("findOccurance() = " + res);
        res = MyCollection.findOccurance(words);
        System.out.println("findOccurance() = " + res);

        // Task 5 check
        System.out.println("5. " + "=".repeat(80));
        PhoneBook phoneBook = new PhoneBook();
        //List<PhoneRecord> records = List.of("Yuriy","0635813000");

        phoneBook.add(new PhoneRecord("Yuriy", "0635813000"));
        phoneBook.add(new PhoneRecord("Yuriy", "0505813000"));
        phoneBook.add(new PhoneRecord("Yuriy", "0675813000"));
        phoneBook.add(new PhoneRecord("Tanya", "0635813005"));
        phoneBook.add(new PhoneRecord("Tanya", "0675813005"));
        phoneBook.add(new PhoneRecord("Lora", "0635813004"));

        System.out.println("find('Lora') = " + phoneBook.find("Lora"));
        System.out.println("find('Tanya') = " + phoneBook.find("Tanya"));
        System.out.println("find('Yuriy') = " + phoneBook.find("Yuriy"));
        System.out.println("find('Yurii') = " + phoneBook.find("Yurii"));

        System.out.println("findAll('Lora') = " + phoneBook.findAll("Lora"));
        System.out.println("findAll('Tanya') = " + phoneBook.findAll("Tanya"));
        System.out.println("findAll('Yuriy') = " + phoneBook.findAll("Yuriy"));
        System.out.println("findAll('Yurii') = " + phoneBook.findAll("Yurii"));
    }
}

