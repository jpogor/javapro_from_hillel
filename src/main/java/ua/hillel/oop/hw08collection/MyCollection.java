package ua.hillel.oop.hw08collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyCollection {

    public static int countOccurance (List<String> stringList, String searchString) {
        int count = 0;
        for (String str : stringList) {
            if (str.equals(searchString)) {
                count++;
            }
        }
        return count;
    }

    public static List<String> toList (String[] arr) {
        var result = new LinkedList<String>();
        for (int i = 0; i < arr.length; i++) {
            result.add(arr[i]);
        }
        return result;
    }

    public static List<String> findUnique (List<String> items) {
        var result = new ArrayList<String>();
        for (String item: items) {
            if (result.contains(item))
                continue;
            //int cnt = countOccurance (items, item);
            result.add(item);
        }
        return result;
    }

    public record StringCounter(String name, int occurrence) {
    }

    public static List<StringCounter> findOccurance (List<String> items) {
        List<StringCounter> resList = new ArrayList<>();
        for (String item : findUnique(items)) {
            int cnt = MyCollection.countOccurance(items, item);
            //if (!resList.contains(new StringCounter(item,cnt)))  // не нужна, т.к. использован findUnique
            resList.add(new StringCounter(item,cnt));
        }
        return resList;
    }
}
