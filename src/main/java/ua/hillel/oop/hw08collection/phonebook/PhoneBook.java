package ua.hillel.oop.hw08collection.phonebook;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {
    private final List<PhoneRecord> recordList = new ArrayList<>();

    public void add(PhoneRecord record) {
        if (!this.recordList.contains(record))
            this.recordList.add(record);
    }

    public void add(List<PhoneRecord> records) {
        for (PhoneRecord record : records) {
            this.add(record);
        }
    }

    public PhoneRecord find(String name) {
        PhoneRecord rec = null;
        for (PhoneRecord record : this.recordList)
            if (record.getName().equals(name))
                rec = record;
        return rec;
    }

    public List<PhoneRecord> findAll(String name) {
        List<PhoneRecord> recs = new ArrayList<>();
        for (PhoneRecord record : this.recordList)
            if (record.getName().equals(name))
                recs.add(record);
        //if (recs.size() == 0)  recs = null; // для колекцій ніколи не повертайте null
        return recs;
    }
}
