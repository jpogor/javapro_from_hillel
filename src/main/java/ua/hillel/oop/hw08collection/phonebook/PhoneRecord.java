package ua.hillel.oop.hw08collection.phonebook;

import java.util.Objects;

public class PhoneRecord {

    private final String name;
    private final String phone;

    public String getName() {
        return name;
    }
    public String getPhone() {
        return phone;
    }

    public PhoneRecord(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneRecord record = (PhoneRecord) o;
        return Objects.equals(name, record.name) && Objects.equals(phone, record.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phone);
    }

    @Override
    public String toString() {
        return "{ name: " + this.name + ", phone: " + this.phone + " }";
    }
}
