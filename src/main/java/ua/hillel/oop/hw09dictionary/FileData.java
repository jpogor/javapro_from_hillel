package ua.hillel.oop.hw09dictionary;

import java.util.Objects;

public class FileData {

    private final String fileName;    // ім'я файлу
    private final int fileSize;       // розміру в байтах
    private final String filePath;    //шлях до файлу

    @Override
    public String toString() {
        return "{" + "fileName: " + fileName + ", fileSize: " + fileSize + ", filePath: '" + filePath + "}";
    }

    public FileData(String fileName, int fileSize, String filePath) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public int getFileSize() {
        return fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileData fileData = (FileData) o;
        return (fileSize == fileData.fileSize) && Objects.equals(fileName, fileData.fileName) && Objects.equals(filePath, fileData.filePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, fileSize, filePath);
    }
}
