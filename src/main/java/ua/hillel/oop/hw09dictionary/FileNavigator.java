package ua.hillel.oop.hw09dictionary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class FileNavigator {
    private final HashMap<String, List<FileData>> files = new HashMap<>();

    @Override
    public String toString() {
        return "{" + "files: " + files + "}";
    }

    public HashMap<String, List<FileData>> add(String filePath, FileData fileData) {

        if (!filePath.equals(fileData.getFilePath()))
            throw new PathException("Error: filePath[" + filePath + "] is not equals of path for files [" + fileData.getFilePath() + "]");

        List<FileData> fls = find(filePath);
        fls.add(fileData);
        files.put(filePath, fls);

        return files;
    }

    public List<FileData> find (String filePath) {

        List<FileData> result = (!files.containsKey(filePath)) ? new ArrayList<>() : files.get(filePath);
        return result;
    }

    public List<FileData> filterBySize(int fileSize) {
        var result = new ArrayList<FileData>();
        for (String key : files.keySet())
            for (FileData fileData : files.get(key))
                if (fileData.getFileSize() <= fileSize)
                    result.add(fileData);
        return result;
    }

    public ArrayList<FileData> remove(String path) {
        ArrayList<FileData> result = new ArrayList<>();
        for (FileData file : files.get(path))
            result.add(file);
        files.remove(path);
        return result;
    }

    public ArrayList<FileData> sortBySize() {
        ArrayList<FileData> result = new ArrayList<>();
        for (String key : files.keySet())
            for (FileData fileData : files.get(key))
                result.add(fileData);
        result.sort(Comparator.comparingInt(FileData::getFileSize));
        return result;
    }
}
