package ua.hillel.oop.hw09dictionary;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        var fileNavigator = new FileNavigator();

        FileData fileData1 = new FileData("test_01.txt", 101, "D:\\Work");
        FileData fileData2 = new FileData("test_02.txt", 202, "D:\\Work_02");
        FileData fileData3 = new FileData("test_03.txt", 101, "D:\\Work");

        Map<String, List<FileData>> dirs = fileNavigator.add("D:\\Work", fileData1);
        System.out.println("add() = " + dirs);
        dirs = fileNavigator.add("D:\\Work_02", fileData2);
        System.out.println("add() = " + dirs);
        dirs = fileNavigator.add("D:\\Work", fileData3);
        System.out.println("add() =" + dirs);

        List<FileData> files = fileNavigator.filterBySize(101);
        System.out.println("filterBySize(101) = " + files);
        files = fileNavigator.filterBySize(202);
        System.out.println("filterBySize(202) = " + files);

        files = fileNavigator.remove("D:\\Work");
        System.out.println("remove() = " + files);

        dirs = fileNavigator.add("D:\\Work", fileData1);
        System.out.println("add() = " + dirs);

        dirs = fileNavigator.add("D:\\Work", fileData3);
        System.out.println("add() =" + dirs);

        files = fileNavigator.sortBySize();
        System.out.println("sortBySize() = " + files);

        try {
            dirs = fileNavigator.add("D:\\Work", fileData2);
            System.out.println(dirs);
        } catch (PathException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
