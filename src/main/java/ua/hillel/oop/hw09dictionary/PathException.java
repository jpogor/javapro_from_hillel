package ua.hillel.oop.hw09dictionary;

public class PathException extends RuntimeException {
    public PathException(String errorMessage) {
        super(errorMessage);
    }

    public PathException(String message, Throwable cause) {
        super(message, cause);
    }
}