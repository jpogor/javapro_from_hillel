package ua.hillel.oop.hw10queue;

import java.util.LinkedList;
import java.util.Queue;

public class CoffeeOrderBoard {
    private final Queue<Order> orders = new LinkedList<>();

    public int getOrderCount() {
        return orderCount;
    }

    private int orderCount = 0;
    @Override
    public String toString() {
        return "{" + "orders: " + orders + "}";
    }

    public boolean add(String customerName) {
        Order order = new Order(++orderCount, customerName);
        return this.orders.add(order);
    }

    public Order deliver() {
        return this.orders.remove();
    }

    public Order deliver(int orderNum) {
        Order result = null;
        for(Order order : this.orders) {
            if(order.getOrderNumber() == orderNum) {
                result = new Order(order);
                orders.remove(order);
                break;
            }
        }
        return result;
    }

    public String draw() {
        String result = "\n" + "=".repeat(20) + "\nNum | Name\n";
        for(Order order : orders)
            result += " " + order.getOrderNumber() + " ".repeat(3-String.valueOf(order.getOrderNumber()).length()) + "| " + order.getCustomerName() + "\n";
        //String result = orders.toString();    // тут интересный вопросик
        //String result = "[{" + "orders: " + orders + "}]";
        return result;
    }
}
