package ua.hillel.oop.hw10queue;

public class Main {
    public static void main(String[] args) {
        CoffeeOrderBoard coffeeOrderBoard = new CoffeeOrderBoard();

        // 3. Реалізувати метод add у класі CoffeeOrderBoard. Даний метод додає нове замовлення та надає замовленню номер (натуральний порядок).
        System.out.println("3. " + "=".repeat(80));
        coffeeOrderBoard.add("Alen");
        System.out.println("add(), coffeeOrderBoard = " + coffeeOrderBoard);
        coffeeOrderBoard.add("Yoda");
        System.out.println("add(), coffeeOrderBoard = " + coffeeOrderBoard);
        coffeeOrderBoard.add("Obi-van");
        System.out.println("add(), coffeeOrderBoard = " + coffeeOrderBoard);
        coffeeOrderBoard.add("John Snow");
        System.out.println("add(), coffeeOrderBoard = " + coffeeOrderBoard);

        // 4. Реалізувати метод deliver у класі CoffeeOrderBoard. Цей метод видає найближче у черзі замовлення. Видача супроводжується видаленням замовлення зі списку.
        System.out.println("4. " + "=".repeat(80));
        Order result = coffeeOrderBoard.deliver();
        System.out.println("deliver(), coffeeOrderBoard = " + coffeeOrderBoard + ", delivered order = " + result);

        // 5. Реалізувати метод deliver у класі CoffeeOrderBoard. Даний метод видає замовлення з певним номером. Видача супроводжується видаленням замовлення зі списку.
        System.out.println("5. " + "=".repeat(80));
        result = coffeeOrderBoard.deliver(3);
        System.out.println("deliver(2), coffeeOrderBoard = " + coffeeOrderBoard + ", delivered order = " + result);

        // 6. Реалізувати метод draw у класі CoffeeOrderBoard. Цей метод виводить у консоль інформацію про поточний стан черги у порядку найближчого до видачі замовлення.
        System.out.println("6. " + "=".repeat(80));
        System.out.println("draw(),     coffeeOrderBoard = " + coffeeOrderBoard.draw());
        System.out.println("toString(), coffeeOrderBoard = " + coffeeOrderBoard);

        System.out.println("=".repeat(83));
    }
}
