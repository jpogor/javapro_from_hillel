package ua.hillel.oop.hw12file;

public interface Logger {
    void debug(String message);
    void info(String message);
}
