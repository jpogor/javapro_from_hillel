package ua.hillel.oop.hw12file;

import java.io.IOException;

public interface LoggerConfigurationLoader {
    LoggerConfiguration load() throws IOException;
}
