package ua.hillel.oop.hw12file;

import ua.hillel.oop.hw12file.file.FileLogger;
import ua.hillel.oop.hw12file.file.FileLoggerConfiguration;
import ua.hillel.oop.hw12file.file.FileLoggerConfigurationLoader;
import ua.hillel.oop.hw12file.stdout.StdoutLogger;
import ua.hillel.oop.hw12file.stdout.StdoutLoggerConfiguration;
import ua.hillel.oop.hw12file.stdout.StdoutLoggerConfigurationLoader;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        var config1 = new FileLoggerConfigurationLoader().load();
        var fileLogger = new FileLogger((FileLoggerConfiguration) config1);
        var config2 = new StdoutLoggerConfigurationLoader().load();
        var stdoutLogger = new StdoutLogger((StdoutLoggerConfiguration) config2);

        for(int i = 0; i < 1000; i++) {
            fileLogger.debug("Debug message " + i);
            stdoutLogger.debug("Debug message " + i);
            fileLogger.info("Info message " + i);
            stdoutLogger.info("Info message " + i);
        }
    }
}
