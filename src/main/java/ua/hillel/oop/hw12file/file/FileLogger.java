package ua.hillel.oop.hw12file.file;


import ua.hillel.oop.hw12file.Logger;
import ua.hillel.oop.hw12file.LoggingLevel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLogger implements Logger {

    private final FileLoggerConfiguration config;

    @Override
    public void debug(String message) {
        if(config.level().equals(LoggingLevel.DEBUG)) {
            var result = createMessage(message, LoggingLevel.DEBUG);
            writeMessage(result);
        }
    }

    @Override
    public void info(String message) {
        if(config.level().equals(LoggingLevel.INFO) || config.level().equals(LoggingLevel.DEBUG)) {
            var result = createMessage(message, LoggingLevel.INFO);
            writeMessage(result);
        }
    }

    public FileLogger(FileLoggerConfiguration config) {
        this.config = config;
    }

    private String createMessage(String message, LoggingLevel level) {
        String output = config.format();    // "[{0}] [{1}] Message: [{2}]";
        output = MessageFormat.format(output, LocalDateTime.now(), level, message);
        return output;
/*
        var output = new StringBuffer();
        output.append("[");
        output.append(LocalDateTime.now());
        output.append("]");
        output.append("[" + level + "]");
        output.append(" Message: [");
        output.append(message);
        output.append("]");
        return output.toString();
*/
    }

    private void writeMessage(String message) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH");
        LocalDateTime dateTime = LocalDateTime.now();

        String pathName = config.path();
        File path = Path.of(pathName).toFile();
        if(!path.exists())
            path.mkdir();

        try {
            File file;
            String fileName;
            for(int i = 0;; i++) {
                String suffix = (i == 0) ? "" : "_" + i;
                fileName = config.path() + "\\" + config.mask() + "_" + dtf.format(dateTime) + suffix + ".log";
                file = Path.of(fileName).toFile();
                if (!file.exists()) {
                    file.createNewFile();
                    break;
                }
                if (file.length() <= config.maxSize()) {
                    break;
                }
            }

            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(fileName, true));
            out.append(message + "\n");
            out.close();
        } catch (IOException err) {
            System.out.println("Error: " + err);
        }
    }
}
