package ua.hillel.oop.hw12file.file;

import ua.hillel.oop.hw12file.LoggerConfiguration;
import ua.hillel.oop.hw12file.LoggingLevel;

public record FileLoggerConfiguration (String path, String mask, LoggingLevel level, int maxSize, String format) implements LoggerConfiguration { }
