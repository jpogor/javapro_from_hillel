package ua.hillel.oop.hw12file.file;

import ua.hillel.oop.hw12file.LoggerConfiguration;
import ua.hillel.oop.hw12file.LoggerConfigurationLoader;
import ua.hillel.oop.hw12file.LoggingLevel;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

public class FileLoggerConfigurationLoader implements LoggerConfigurationLoader {

    @Override
    public LoggerConfiguration load() throws IOException {
        FileInputStream fis = new FileInputStream("./src/main/resources/file-loader.properties");

        var bytes = fis.readAllBytes();
        var content = new String(bytes);
        var lines = content.split("\r\n");

        var params = new HashMap<String,String>();
        for(int i = 0; i < lines.length; i++) {
            var pair = lines[i].split(":", 2);
            params.put(pair[0].toUpperCase().trim(), pair[1].trim());
        }
/*
        String path = params.get("PATH");
        String mask = params.get("MASK");
        LoggingLevel level = LoggingLevel.valueOf(params.get("LEVEL"));
        int maxSize = Integer.parseInt(params.get("MAXSIZE"));
        String format = params.get("FORMAT");
*/
        var result = new FileLoggerConfiguration(params.get("PATH"),
                                                 params.get("MASK"),
                                                 LoggingLevel.valueOf(params.get("LEVEL")),
                                                 Integer.parseInt(params.get("MAXSIZE")),
                                                 params.get("FORMAT"));
        return result;
    }
}
