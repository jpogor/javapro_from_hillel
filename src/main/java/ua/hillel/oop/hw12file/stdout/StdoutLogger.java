package ua.hillel.oop.hw12file.stdout;

import ua.hillel.oop.hw12file.Logger;
import ua.hillel.oop.hw12file.LoggingLevel;

import java.text.MessageFormat;
import java.time.LocalDateTime;

public class StdoutLogger implements Logger {

    private final StdoutLoggerConfiguration config;

    public StdoutLogger(StdoutLoggerConfiguration config) {
        this.config = config;
    }

    @Override
    public void debug(String message) {
        if(config.level().equals(LoggingLevel.DEBUG)) {
            var result = createMessage(message, LoggingLevel.DEBUG);
            System.out.println(result);
        }
    }

    @Override
    public void info(String message) {
        if(config.level().equals(LoggingLevel.INFO) || config.level().equals(LoggingLevel.DEBUG)) {
            var result = createMessage(message, LoggingLevel.INFO);
            System.out.println(result);
        }
    }

    private String createMessage(String message, LoggingLevel level) {
        String output = config.format();    // "[{0}] [{1}] Message: [{2}]";
        output = MessageFormat.format(output, LocalDateTime.now(), level, message);
        return output;
    }
}
