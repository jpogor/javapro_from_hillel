package ua.hillel.oop.hw12file.stdout;

import ua.hillel.oop.hw12file.LoggerConfiguration;
import ua.hillel.oop.hw12file.LoggingLevel;

public record StdoutLoggerConfiguration(LoggingLevel level, String format) implements LoggerConfiguration {
    public StdoutLoggerConfiguration(LoggingLevel level, String format) {
        this.level = level;
        this.format = format;
    }
}
