package ua.hillel.oop.hw14generic;

import ua.hillel.oop.hw14generic.box.Box;
import ua.hillel.oop.hw14generic.fruit.Apple;
import ua.hillel.oop.hw14generic.fruit.Fruit;
import ua.hillel.oop.hw14generic.fruit.Orange;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Box box1 = new Box();

        Integer[] intArray = {1, 2, 3, 4, 5};
        List<Integer> itemsInt = box1.toList(intArray);
        System.out.println(itemsInt);

        String[] strArray = {"one", "two", "three", "for", "five"};
        List<String> itemsStr = box1.toList(strArray);
        System.out.println(itemsStr);

        Box<Apple> boxApple = new Box();
        Box<Orange> boxOrange = new Box();

        Apple apple1 = new Apple();
        boolean result = boxApple.add(apple1);
        System.out.println("Add apple to boxApple, result = " + result + ", Box Weight = " + boxApple.getWeight() + ", count = " + boxApple.size());

        //Orange orange1 = new Orange();
        //result = boxApple.add(orange1);
        //System.out.println("Add orange to boxApple, result = " + result + ", Box Weight = " + boxApple.getWeight() + ", count = " + boxApple.size());

        List<Apple> fruits1 = List.of(new Apple(), new Apple());
        result = boxApple.add(fruits1);
        System.out.println("Add(2 apples) to box Apple, result = " + result + ", Box Weight = " + boxApple.getWeight() + ", count = " + boxApple.size());

        List<Orange> fruits2 = List.of(new Orange(), new Orange());
        result = boxOrange.add(fruits2);
        System.out.println("Add(2 oranges) to box Orange, result = " + result + ", Box Weight = " + boxOrange.getWeight() + ", count = " + boxOrange.size());

        result = boxApple.compare(boxOrange);
        System.out.println("Compare boxes Apple to boxes Orange, result = " + result);

        System.out.println("merge(start),          boxApple   , Box Weight = " + boxApple.getWeight() + ", count = " + boxApple.size());
        Box<Apple> boxAppleNew = new Box();
        boxAppleNew = boxAppleNew.merge(boxApple);
        System.out.println("merge(from  boxApple), boxApple   , Box Weight = " + boxApple.getWeight() + ", count = " + boxApple.size());
        System.out.println("merge(to boxAppleNew), boxAppleNew, Box Weight = " + boxAppleNew.getWeight() + ", count = " + boxAppleNew.size());
    }
}
