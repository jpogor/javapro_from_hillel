package ua.hillel.oop.hw14generic.box;

import ua.hillel.oop.hw14generic.fruit.Apple;
import ua.hillel.oop.hw14generic.fruit.Fruit;
import ua.hillel.oop.hw14generic.fruit.Orange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Box<T extends Fruit> {

    public <T> List<T> getFruits() {
        return (List<T>) fruits;
    }

    private final List<T> fruits = new ArrayList<>();

    public <E> List<E> toList(E[] a) {
        return Arrays.stream(a).collect(Collectors.toList());
    }

    public boolean add(T fruit) {
        if (fruits.isEmpty())
            return fruits.add(fruit);

        if (!fruit.getClass().equals(fruits.get(0).getClass())) // сравнение оставил на случай если пип при вызове будет обьявлен неправильно
            return false;

        return fruits.add(fruit);
    }

    public boolean add(List<T> fruits) {
        boolean result = false;
        for(T fruit : fruits) {
            result = this.fruits.add(fruit);
            if(result == false)
                break;
        }
        return result;
    }

    public float getWeight() {
        if(fruits.isEmpty())
            return 0F;
        float fruitWeight = fruits.get(0).getWeight();
        return fruits.size() * fruitWeight;
/*
        float fruitWeight = 0F;
        if(fruits.get(0).getClass().equals(Apple.class)) {
            Apple apple = (Apple) fruits.get(0);
            fruitWeight = apple.getWeight();
        } else {
            Orange orange = (Orange) fruits.get(0);
            fruitWeight = orange.getWeight();
        }
*/
    }

    public boolean compare(Box box) {
        return (this.getWeight() == box.getWeight());
    }

    public Box<T> merge(Box<T> box) {
        Box result = new Box();
        result.add(box.fruits);
        box.fruits.clear();
        return result;
    }

    public int size() {
        return fruits.size();
    }
}
