package ua.hillel.oop.hw14generic.fruit;

public class Apple extends Fruit {
    public Apple() {
        super.setWeight(1.0F);
    }
}
