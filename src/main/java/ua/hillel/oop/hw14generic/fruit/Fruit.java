package ua.hillel.oop.hw14generic.fruit;

public class Fruit {
    private float weight = 0;

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getWeight() {
        return weight;
    }
}
