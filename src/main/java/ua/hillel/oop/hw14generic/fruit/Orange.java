package ua.hillel.oop.hw14generic.fruit;

public class Orange extends Fruit {
    public Orange() {
        super.setWeight(1.5F);
    }
}
