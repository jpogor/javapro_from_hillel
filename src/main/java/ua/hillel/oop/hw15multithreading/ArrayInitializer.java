package ua.hillel.oop.hw15multithreading;

import java.util.ArrayList;
import java.util.List;

public class ArrayInitializer {

    protected static final int NUMBER_OF_PARTS = 2;

    public static void init(double[] array) {
        if(array.length == 0)
            return;

        List<Thread> threads = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_PARTS; i++) {
            threads.add(new Thread(new CalcArrayParts(array, i), "thread-" + i));
        }

        for (Thread thread : threads)
            thread.start();

        try {
            for (Thread thread : threads)
                thread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    protected static void /*Runnable*/ fillPartOfArray(double[] array, int numPart) {
        int offset = (numPart - 1) * array.length / NUMBER_OF_PARTS;
        int partLen = (numPart < NUMBER_OF_PARTS) ? (array.length / NUMBER_OF_PARTS) : (array.length - offset);
        for(int i = 0; i < partLen; i++) {
            System.out.println("Thread name: " + Thread.currentThread().getName() + ", >>> start, array[" + (i + offset) + "] = " + array[i + offset]);
            array[i + offset] *= (Math.sin(0.2 + i / 5.0) * Math.cos(0.2 + i / 5.0) * Math.cos(0.4 + i / 2.0));
            System.out.println("Thread name: " + Thread.currentThread().getName() + ", <<<   end, array[" + (i + offset) + "] = " + array[i + offset]);
        }
        //return null;
    }

    private static class CalcArrayParts implements Runnable {
        double[] array;
        int numPart;

        public CalcArrayParts(double[] array, int numPart) {
            this.array = array;
            this.numPart = numPart;
        }

        @Override
        public void run() {
            fillPartOfArray(array, numPart);
        }
    }
}
