package ua.hillel.oop.hw15multithreading;

public class Main {
    public static void main(String[] args) {
        double[] array = new double[101];
        for (int i = 0; i < array.length; i++)
            array[i] = Math.PI;
        ArrayInitializer.init(array);
    }
}
