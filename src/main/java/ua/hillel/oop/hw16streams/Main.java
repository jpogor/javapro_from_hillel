package ua.hillel.oop.hw16streams;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        Products products = new Products();
        products.add(new Product("Book", 255.0, 5, LocalDate.parse("2022-04-03")));
        products.add(new Product("Book", 300.0, 10, LocalDate.parse("2020-10-15")));
        products.add(new Product("Journal", 30.0, 0, LocalDate.parse("2021-08-25")));
        products.add(new Product("Toy", 240.0, 0, LocalDate.parse("2017-03-14")));
        products.add(new Product("Book", 260.0, 15, LocalDate.parse("2015-09-27")));
        products.add(new Product("Book", 75.0, 10,  LocalDate.parse("2022-07-09")));
        products.add(new Product("Toy", 80.0, 0, LocalDate.parse("2020-02-13")));
        products.add(new Product("Book", 320.0, 25, LocalDate.parse("2019-12-16")));
        products.add(new Product("Journal", 55.0, 10,  LocalDate.parse("2021-04-04")));
        products.add(new Product("Book", 175.0, 0, LocalDate.parse("2022-11-17")));
        products.add(new Product("Toy", 55.0, 0, LocalDate.parse("2020-08-01")));

        System.out.println(products);
        System.out.println("-".repeat(80));

        // 1. Реалізувати метод отримання всіх продуктів у вигляді списку, категорія яких еквівалентна “Book” та ціна більш ніж 250.
        System.out.println("getExpensive() = " + products.getExpensive("Book", 250));
        System.out.println("-".repeat(80));

        // 2. Реалізувати метод отримання всіх продуктів як списку, категорія яких еквівалентна “Book” і з можливістю застосування знижки.
        // Фінальний список повинен містити продукти з застосованою знижкою 10%.
        // Так, якщо Продукт A був з ціною 1.0 USD, то його фінальна ціна залишатиметься 0.9 USD
        System.out.println("getDiscount() = " + products.getDiscount("Book", 10));
        System.out.println("-".repeat(80));

        // 3. Реалізувати метод отримання найдешевшого продукту з категорії “Book”
        // У випадку, якщо жоден продукт не знайдено (ситуація, коли немає продукту з категорією), викинути виняток.
        // `Stream.min/sorted та Optional.orElseThrow() можуть допомогти`
        System.out.println("getCategoryCheapest_v1() = " + products.getCheapest_v1("Book"));
        System.out.println("-".repeat(80));
        System.out.println("getCategoryCheapest_v2() = " + products.getCheapest_v2("Book"));
        System.out.println("-".repeat(80));

        //4. Реалізувати метод отримання трьох останніх доданих продуктів
        System.out.println("getLastCreated() = " + products.getLastCreated(3));
        System.out.println("-".repeat(80));

        //5. Реалізувати метод калькуляції загальної вартості продуктів, які відповідають наступним критеріям:
        //- продукт додано протягом поточного року
        //- продукт має тип “Book”
        //- ціна продукту не перевищує 75
        System.out.println("doCalc() = " + products.doCalc("Book", 2022, 175));
        System.out.println("-".repeat(80));

        // 6. Реалізувати метод групування об'єктів за типом продукту.
        // Таким чином результатом виконання методу буде тип даних “Словник”,
        // що зберігає пару ключ-значення: {тип: список_продуктів}
        System.out.println("doGroup() = " + products.doGroup());
        System.out.println("-".repeat(80));
    }
}
