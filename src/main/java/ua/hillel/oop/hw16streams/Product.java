package ua.hillel.oop.hw16streams;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public record Product(String category, Double price, int discount, LocalDate created) {

    @Override
    public String toString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String s = "\"category\": " + category + ", \"price\": " + price + ", \"discount\": " + discount + "%, \"created\": " + created;
        return s;
    }
}
