package ua.hillel.oop.hw16streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Products {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Products products1 = (Products) o;

        return products.equals(products1.products);
    }

    @Override
    public int hashCode() {
        return products.hashCode();
    }

    private final List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }

    public String toString() {
        String s = "";
        for (Product p : products) {
            s += p.toString(); // + "\n";
        }
        return s;
    }

    public void add(Product product) {
        products.add(product);
    }

    public List<Product> getExpensive(String category, double minPrice) {
        return products.stream()
                .filter(product -> product.category().equals(category))
                .filter(product -> product.price() > minPrice)
                .toList();
    }

    public List<Product> getDiscount(String category, int minDiscount) {
        return products.stream()
                .filter(product -> product.category().equals(category))
                .filter(product -> product.discount() >= minDiscount)
                .map(product -> new Product(product.category(), (product.price() * (100 - product.discount())) / 100, product.discount(), product.created()))
                .toList();
    }

    public List<Product> getCheapest_v1(String category) {
        return products.stream()
                .filter(product -> product.category().equals(category))
                .sorted((p1, p2) -> (int) (p1.price() * 100 * (100 - p1.discount())) - (int) (p2.price() * 100 * (100 - p2.discount())))
                .limit(1)
                .toList();
    }

    public Product getCheapest_v2(String category) {
        return products.stream()
                .filter(product -> product.category().equals(category))
                .min(Comparator.comparingDouble(Product::price))
                .orElseThrow(() -> new RuntimeException("There are no product in the category " + category));
    }

    public List<Product> getLastCreated(int num) {
        return products.stream()
                //.filter(product -> product.category().equals(category))
                .sorted((p1, p2) -> p2.created().compareTo(p1.created()))
                .limit(num)
                .toList();
    }

    public double doCalc(String category, int year, int maxPrice) {
        return products.stream()
                .filter(product -> product.category().equals(category))
                .filter(product -> product.created().getYear() == year)
                .filter(product -> product.price() <= maxPrice)
                .mapToDouble(Product::price)
                .sum();
    }

    public Map<String, List<Product>> doGroup() {
        return products.stream()
                .collect(Collectors.groupingBy(Product::category, Collectors.toList()));
        }
}

/*
public class Products<T extends Product> {

    private List<T> products = new ArrayList<>();

    // 1. Реалізувати метод отримання всіх продуктів у вигляді списку, категорія яких еквівалентна “Book” та ціна більш ніж 250.
    public List<T> getCategoryExpensivePrice(String category, double minPrice) {
        var result = products.stream()
                .filter(product -> product.category().equals(category))
                .filter(product -> product.price() > minPrice).toList();
        return result;
    }

    public void add(Product product) {
        products.add((T) product);
    }
}
*/