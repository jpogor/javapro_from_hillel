package ua.hillel.oop.hw17multithreading;

import ua.hillel.oop.hw15multithreading.ArrayInitializer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ArrayInitializer2  extends ArrayInitializer {

    public static void init(double[] array) {
        if(array.length == 0)
            return;

        var tasks = new ArrayList<CalcArrayParts2>();
        for (int i = 0; i < NUMBER_OF_PARTS; i++) {
            tasks.add(new CalcArrayParts2(array, i+1));
            tasks.get(i).fork();
        }
        for (var task : tasks)
            task.join();
/*
        var l = new CalcArrayParts2(array, 1);
        var r = new CalcArrayParts2(array, 2);
        l.fork();
        r.fork();
        l.join();
        r.join();
*/
    }
    private static class CalcArrayParts2 extends RecursiveTask<Integer> {
        double[] array;
        int numPart;

        public CalcArrayParts2(double[] array, int numPart) {
            this.array = array;
            this.numPart = numPart;
        }

        @Override
        protected Integer compute() {
            fillPartOfArray(array, numPart);
            return null;
        }
    }
}
