package ua.hillel.oop.hw17multithreading;

import static ua.hillel.oop.hw17multithreading.ArrayInitializer2.*;

public class Main {
    public static void main(String[] args) {
        int sum = SimpleCalculator.squareSum(12, 18);
        System.out.println(sum);
        System.out.println("=".repeat(100));

        double[] array = new double[1001];
        for (int i = 0; i < array.length; i++)
            array[i] = Math.PI;
        init(array);
        System.out.println("=".repeat(100));


    }
}
