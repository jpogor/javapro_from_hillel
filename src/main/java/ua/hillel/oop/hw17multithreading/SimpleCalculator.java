package ua.hillel.oop.hw17multithreading;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class SimpleCalculator {
    public static int squareSum(int first, int second) {
        CompletableFuture<Integer> futureFirst = CompletableFuture.supplyAsync(() -> first * first);
        CompletableFuture<Integer> futureSecond = CompletableFuture.supplyAsync(() -> second * second);
        CompletableFuture<Integer> result = futureFirst.thenCombine(futureSecond, (a, b) -> a + b);

        try {
            return result.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
