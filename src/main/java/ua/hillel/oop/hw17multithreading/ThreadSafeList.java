package ua.hillel.oop.hw17multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.*;

public class ThreadSafeList<T> {

    private final List<T> items = new ArrayList<T>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void add(T value) {
        Lock curLock = this.lock.writeLock();
        curLock.lock();

        try {
            items.add(value);
        } finally {
            curLock.unlock();
        }
    }

    public T remove(int index) {
        Lock curLock = lock.writeLock();
        curLock.lock();

        try {
            return items.remove(index);
        } finally {
            curLock.unlock();
        }
    }

    public T get(int index) {
        Lock curLock = lock.readLock();
        curLock.lock();

        try {
            return items.get(index);
        } finally {
            curLock.unlock();
        }
    }
}
