package ua.hillel.oop.hw18lambda.hero;

public record Hero(
        String name // ім'я
       ,String gender // стать
       ,String eyeColor // колір очей
       ,String race // раса
       ,String hairColor // колір волосся
       ,Double height // зріст
       ,String publisher  // видавець
       ,String skinColor // колір шкіри
       ,String alignment // добро / зло
       ,int weight // вага
       ) {

}
