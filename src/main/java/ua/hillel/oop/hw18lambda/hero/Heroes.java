package ua.hillel.oop.hw18lambda.hero;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class Heroes {

    private List<Hero> heroes = new ArrayList<>();

    public Heroes() {
    }
    public Heroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Heroes heroes1 = (Heroes) o;

        return heroes.equals(heroes1.heroes);
    }

    @Override
    public int hashCode() {
        return heroes.hashCode();
    }

    public void add(Hero hero) {
        heroes.add(hero);
    }

    //Середній зріст героїв (виключаючи зріст тих, який нуль або менше)
    public double getAverageAgeOfHeroes() {
        return heroes.stream()
                .filter(hero -> hero.height() > 0)
                .mapToDouble(Hero::height)
                .average()
                .orElseThrow(() -> new RuntimeException("List of Heroes is empty"));
    }

    //Ім’я найвищого героя
    public String getNameOfTheTallestHero() {
        return heroes.stream()
                .max(Comparator.comparingDouble(Hero::height))
                .orElseThrow(() -> new RuntimeException("Heroes list is empty!"))
                .name();
    }

    //Ім’я самого важкого героя
    public String getNameOfTheHeavestHero() {
        return heroes.stream()
                .max(Comparator.comparingDouble(Hero::weight))
                .orElseThrow(() -> new RuntimeException("Heroes list is empty!"))
                .name();
    }

    //Кількість осіб в кожній гендерній групі
    public Map<String, Long> getNumberOfHeroesInEachGenderGroup() {
        return heroes.stream()
                .collect(Collectors.groupingBy(Hero::gender, Collectors.counting()));
    }

    //Кількість осіб в кожному угрупуванні (добро / зло / інші)
    public Map<String, Long> getNumberOfHeroesInEachGroup() {
        return heroes.stream()
                .collect(Collectors.groupingBy(Hero::alignment, Collectors.counting()));
    }

    //5 назв самих популярних видавців
    public List<String> getNamesOfMostPopularPublishers(int cnt) {
        return heroes.stream()
                .collect(Collectors.groupingBy(Hero::publisher, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .limit(cnt)
                .map(Map.Entry::getKey)
                .toList();
    }

    //3 назви найрозповсюдженіших кольорів волосся
    public List<String> getMostCommonHairColors(int cnt) {
        return heroes.stream()
                .collect(Collectors.groupingBy(Hero::hairColor, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .limit(cnt)
                .map(Map.Entry::getKey)
                .toList();
    }

    //Найрозповсюдженіший колір очей
    public List<String> getMostCommonEyeColor(int cnt) {
        return heroes.stream()
                .collect(Collectors.groupingBy(Hero::eyeColor, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .limit(cnt)
                .map(Map.Entry::getKey)
                .toList();
    }

    public static List<Hero> loadHeroes(String csvFile) {
        InputStream inputStream = Heroes.class.getClassLoader().getResourceAsStream(csvFile);
        if (inputStream == null)
            return new ArrayList<>();
        var scanner = new Scanner(inputStream);
        List<Hero> heroes = new ArrayList<>();
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] heroFields = line.split(";");
            if (heroFields[0].equals("Id"))
                continue;
            double weight;
            if (heroFields[6].contains(","))
                weight = Double.parseDouble(heroFields[6].replace(",", "."));
            else
                weight = Double.parseDouble(heroFields[6]);
            heroes.add(new Hero(heroFields[1],
                    heroFields[2],
                    heroFields[3],
                    heroFields[4],
                    heroFields[5],
                    weight,
                    heroFields[7],
                    heroFields[8],
                    heroFields[9],
                    Integer.parseInt(heroFields[10])));
        }
        return heroes;
    }
}
