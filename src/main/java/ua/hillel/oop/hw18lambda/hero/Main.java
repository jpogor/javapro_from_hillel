package ua.hillel.oop.hw18lambda.hero;

public class Main {
    public static void main(String[] args) {

        Heroes heroes = new Heroes();
        heroes.add(new Hero("Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110));
        heroes.add(new Hero("Hero_02", "Male", "Red", "Black", "Black", 100.10, "Elephant", "Bronse", "Good", 55));
        heroes.add(new Hero("Hero_03", "Female", "Brown", "Brown", "Brown", 180.00, "Marvell", "Bronse", "Good", 210));
        heroes.add(new Hero("Hero_04", "Male", "Blue", "Black", "Black", 170.00, "Marvell", "Bronse", "Evil", 100));
        heroes.add(new Hero("Hero_05", "Male", "Blue", "Red", "Black", 210.00, "Marvell", "Bronse", "Good", 270));
        heroes.add(new Hero("Hero_06", "Female", "Rad", "Black", "Black", 188.00, "Marvell", "Bronse", "Good", 95));
        heroes.add(new Hero("Hero_07", "Male", "Blue", "Black", "Black", 95.00, "Cartoon", "Bronse", "Evil", 120));
        heroes.add(new Hero("Hero_08", "Male", "Brown", "Brown", "Brown", 195.00, "Marvell", "Bronse", "Good", 190));
        heroes.add(new Hero("Hero_09", "Female", "Blue", "Red", "Black", 155.00, "Elephant", "Bronse", "Good", 140));
        heroes.add(new Hero("Hero_10", "Male", "Blue", "Black", "Black", 177.00, "Marvell", "Bronse", "Evil", 185));
        heroes.add(new Hero("Hero_11", "Female", "Blue", "Black", "Black", 166.00, "Marvell", "Bronse", "Good", 106));
        heroes.add(new Hero("Hero_12", "Male", "Rad", "Red", "Black", 112.00, "Cartoon", "Bronse", "Good", 150));
        heroes.add(new Hero("Hero_13", "Male", "Blue", "Black", "Black", 115.00, "Marvell", "Bronse", "Good", 125));
        heroes.add(new Hero("Hero_14", "Male", "Blue", "Black", "Black", 220.00, "Cartoon", "Bronse", "Evil", 177));
        heroes.add(new Hero("Hero_15", "Female", "Brown", "Brown", "Brown", 200.00, "Marvell", "Bronse", "Good", 255));

        System.out.println(heroes);
        System.out.println("-".repeat(80));

        System.out.println("getAverageAgeOfHeroes() = " + heroes.getAverageAgeOfHeroes());
        System.out.println("-".repeat(80));

        System.out.println("getNameOfTheGreatestHero() = " + heroes.getNameOfTheTallestHero());
        System.out.println("-".repeat(80));

        System.out.println("getNameOfTheHardestHero() = " + heroes.getNameOfTheHeavestHero());
        System.out.println("-".repeat(80));

        System.out.println("getNumberOfHeroesInEachGenderGroup() = " + heroes.getNumberOfHeroesInEachGenderGroup());
        System.out.println("-".repeat(80));

        System.out.println("getNumberOfHeroesInEachGroup() = " + heroes.getNumberOfHeroesInEachGroup());
        System.out.println("-".repeat(80));

        System.out.println("getNamesOfMostPopularPublishers(2) = " + heroes.getNamesOfMostPopularPublishers(2));
        System.out.println("-".repeat(80));

        System.out.println("getMostCommonHairColors(1) = " + heroes.getMostCommonHairColors(1));
        System.out.println("-".repeat(80));

        System.out.println("getMostCommonHairColors(2) = " + heroes.getMostCommonEyeColor(2));
        System.out.println("-".repeat(80));

        var heroesCSV = new Heroes(Heroes.loadHeroes("heroes.csv"));
        System.out.println("average height: " + heroesCSV.getAverageAgeOfHeroes());
        System.out.println("the tallest: " + heroesCSV.getNameOfTheTallestHero());
        System.out.println("the heaviest: " + heroesCSV.getNameOfTheHeavestHero());
        System.out.println("gender counts: " + heroesCSV.getNumberOfHeroesInEachGenderGroup());
        System.out.println("alignment counts: " + heroesCSV.getNumberOfHeroesInEachGroup());
        System.out.println("5 most popular publisher: " + heroesCSV.getNamesOfMostPopularPublishers(5));
        System.out.println("3 most popular hair color: " + heroesCSV.getMostCommonHairColors(3));
        System.out.println("the most popular eye color: " + heroesCSV.getMostCommonEyeColor(1));
    }
}
