package ua.hillel.oop.hw19quiksort;

public class Main {
    public static void main(String[] args) {
        int[] a = { 15, 38, 21, 7, 77, 64, 51, 19, 7 };
        System.out.println("\nBefore sort: ");
        var q1 = new QuikSort();
        q1.printArray(a);

        q1.execute(a, 0, a.length - 1);
        System.out.println("\nAfter sort: ");
        q1.printArray(a);

        System.out.println("\n" + "-".repeat(80));
    }
}
