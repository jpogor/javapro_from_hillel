package ua.hillel.oop.hw19quiksort;

public class QuikSort {
    int partition (int[] a, int start, int end) {
        int pivot = a[end];
        int i = (start - 1);

        for (int j = start; j <= end - 1; j++) {
            if (a[j] < pivot) {
                i++;
                int t = a[i];
                a[i] = a[j];
                a[j] = t;
            }
        }
        int t = a[i+1];
        a[i+1] = a[end];
        a[end] = t;
        return (i + 1);
    }

    void execute(int[] a, int start, int end) {
        if (start < end) {
            int p = partition(a, start, end);
            execute(a, start, p - 1);
            execute(a, p + 1, end);
        }
    }

    void printArray(int[] a) {
        for (int i = 0; i < a.length; i++)
            System.out.print(a[i] + ", ");
    }
}
