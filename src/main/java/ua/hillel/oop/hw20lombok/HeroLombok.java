package ua.hillel.oop.hw20lombok;

import lombok.*;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
public class HeroLombok {
    String name;
    String gender;
    String eyeColor;
    String race;
    String hairColor;
    double height;
    String publisher;
    String skinColor;
    String alignment;
    int weight;
}
