package ua.hillel.oop.hw20lombok;

import lombok.Value;

@Value
public final class HeroValue {
    private final String name;
    private final String gender;
    private final String eyeColor;
    private final String race;
    private final String hairColor;
    private final double height;
    private final String publisher;
    private final String skinColor;
    private final String alignment;
    private final int weight;
}
