package ua.hillel.oop.hw24jdbc;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder (access = AccessLevel.PUBLIC)
@Getter

public class Hero {
    private Long id;
    private String name;
    private String gender;
    private String eyeColor;
    private String race;
    private String hairColor;
    private double height;
    private String publisher; //або Long publisherId
    private String skinColor;
    private String alignment;
    private int weight;

    public Hero(Long id, String name, String gender, String eyeColor, String race, String hairColor, double height, String publisher, String skinColor, String alignment, int weight) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.eyeColor = eyeColor;
        this.race = race;
        this.hairColor = hairColor;
        this.height = height;
        this.publisher = publisher;
        this.skinColor = skinColor;
        this.alignment = alignment;
        this.weight = weight;
    }

    public Hero(String name) {
        this.name = name;
    }
}
