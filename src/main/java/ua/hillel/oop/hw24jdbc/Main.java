package ua.hillel.oop.hw24jdbc;

import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var dataSource = dataSource();
      //var dataSource = hikariDataSource();
        var heroDaoImpl = new HeroDaoImpl(dataSource);
        List<Hero> heroes = List.of(
                new Hero(0L, "Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110),
                new Hero(0L, "Hero_02", "Male", "Red", "Black", "Black", 100.10, "Elephant", "Bronse", "Good", 55),
                new Hero(0L, "Hero_03", "Female", "Brown", "Brown", "Brown", 180.00, "Marvell", "Bronse", "Good", 210),
                new Hero(0L, "Hero_04", "Male", "Blue", "Black", "Black", 170.00, "Marvell", "Bronse", "Evil", 100),
                new Hero(0L, "Hero_05", "Male", "Blue", "Red", "Black", 210.00, "Marvell", "Bronse", "Good", 270),
                new Hero(0L, "Hero_06", "Female", "Rad", "Black", "Black", 188.00, "Marvell", "Bronse", "Good", 95),
                new Hero(0L, "Hero_07", "Male", "Blue", "Black", "Black", 95.00, "Cartoon", "Bronse", "Evil", 120),
                new Hero(0L, "Hero_08", "Male", "Brown", "Brown", "Brown", 195.00, "Marvell", "Bronse", "Good", 190),
                new Hero(0L, "Hero_09", "Female", "Blue", "Red", "Black", 155.00, "Elephant", "Bronse", "Good", 140),
                new Hero(0L, "Hero_10", "Male", "Blue", "Black", "Black", 177.00, "Marvell", "Bronse", "Evil", 185),
                new Hero(0L, "Hero_11", "Female", "Blue", "Black", "Black", 166.00, "Marvell", "Bronse", "Good", 106),
                new Hero(0L, "Hero_12", "Male", "Rad", "Red", "Black", 112.00, "Cartoon", "Bronse", "Good", 150),
                new Hero(0L, "Hero_13", "Male", "Blue", "Black", "Black", 115.00, "Marvell", "Bronse", "Good", 125),
                new Hero(0L, "Hero_14", "Male", "Blue", "Black", "Black", 220.00, "Cartoon", "Bronse", "Evil", 177),
                new Hero(0L, "Hero_15", "Female", "Brown", "Brown", "Brown", 200.00, "Marvell", "Bronse", "Good", 255)
        );
        heroes.forEach(heroDaoImpl::create);

        System.out.println("== findAll() " + "=".repeat(80));
        heroDaoImpl.findAll().forEach(System.out::println);

//        System.out.println(heroDaoImpl.findById(2L).orElseThrow());
        System.out.println("== findByName(\"Hero_06\") " + "=".repeat(80));
        System.out.println(heroDaoImpl.findByName("Hero_06"));

        System.out.println("== delete(Hero_14) " + "=".repeat(80));
        heroDaoImpl.findByName("Hero_14").forEach(hero -> System.out.println(heroDaoImpl.delete(hero.getId())));

        //System.out.println("== delete ALL " + "=".repeat(80));
        //heroDaoImpl.findAll().forEach(hero -> System.out.println(heroDaoImpl.delete(hero.getId())));
    }

    private static DataSource dataSource() {
        var ds = new PGSimpleDataSource();
//      ds.setServerNames(new String[]{"localhost"});
        ds.setDatabaseName("postgres");
        ds.setUser("hillel");
        ds.setPassword("hillel");
        return ds;
    }
/*
    private static DataSource hikariDataSource() {
        var config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("hillel");
        config.setPassword("hillel");
        return new HikariDataSource(config);
    }
*/
}
