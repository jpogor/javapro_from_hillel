package ua.hillel.oop.hw25patterns;

import ua.hillel.oop.hw24jdbc.Hero;
import javax.sql.DataSource;
import java.util.List;

public interface Fabric {
    HeroService createService(DataSource dataSource);
    HeroService createService(List<Hero> heroes);
}

