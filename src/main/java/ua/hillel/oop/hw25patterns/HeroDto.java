package ua.hillel.oop.hw25patterns;

import lombok.AllArgsConstructor;

import java.util.List;

public record  HeroDto (Long id, String name, String gender, List<String> movies) {

    public static class HeroDtoBuilder {
        private String name;
        private Long id;
        private String gender;
        private List<String> movies;

        public HeroDtoBuilder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public HeroDtoBuilder name(String name) {
            this.name = name;
            return this;
        }

        public HeroDtoBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public HeroDtoBuilder movies(List<String> movies) {
            this.movies = movies;
            return this;
        }

        public HeroDto build() {
            return new HeroDto(id, name, gender, movies);
        }
    }
}
