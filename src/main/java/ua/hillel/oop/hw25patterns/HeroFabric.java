package ua.hillel.oop.hw25patterns;

import org.postgresql.ds.PGSimpleDataSource;
import ua.hillel.oop.hw24jdbc.Hero;
import ua.hillel.oop.hw24jdbc.HeroDaoImpl;

import javax.sql.DataSource;
import java.util.List;

public class HeroFabric implements Fabric {

    HeroMovieService heroMovieService;

    public HeroFabric(HeroMovieService heroMovieService) {
        this.heroMovieService = heroMovieService;
    }

    @Override
    public HeroService createService(DataSource dataSource) {
        HeroService result = new HeroService(new HeroDaoImpl(createDataSource()), heroMovieService);
        return result;
    }

    @Override
    public HeroService createService(List<Hero> heroes) {
        return new HeroService(new DummyHeroDao(heroes), heroMovieService);
    }

    public static DataSource createDataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{"localhost"});
        dataSource.setDatabaseName("postgres");
        dataSource.setUser("hillel");
        dataSource.setPassword("hillel");
        return dataSource;
    }
}
