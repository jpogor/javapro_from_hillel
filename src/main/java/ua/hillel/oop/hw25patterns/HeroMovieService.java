package ua.hillel.oop.hw25patterns;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HeroMovieService {
    private final Map<String, List<String>> heroMovies;

    public HeroMovieService(Map<String, List<String>> heroMovies) {
        this.heroMovies = heroMovies;
    }

    public List<String> getPlayedIn(String heroName) {
        return heroMovies.get(heroName);
    }

    public Map<String, List<String>> heroMovies() {
        return heroMovies;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (HeroMovieService) obj;
        return Objects.equals(this.heroMovies, that.heroMovies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(heroMovies);
    }

    @Override
    public String toString() {
        return "HeroMovieService[" +
                "heroMovies=" + heroMovies + ']';
    }

}
