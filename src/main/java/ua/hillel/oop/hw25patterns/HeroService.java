package ua.hillel.oop.hw25patterns;

import ua.hillel.oop.hw24jdbc.Hero;
import ua.hillel.oop.hw24jdbc.HeroDao;

import java.util.List;

public record HeroService (HeroDao heroDao, HeroMovieService heroMovieService) {

    public List<HeroDto> getHeroes() {
        return heroDao.findAll().stream()
                //.map(this::fromHeroToHeroDTO)
                .map(hero -> new HeroDto(hero.getId(), hero.getName(), hero.getGender(), heroMovieService.getPlayedIn(hero.getName())))
                .toList();
    }

    public List<HeroDto> getHeroesByName(String name) {
        return heroDao.findAll().stream()
                .filter(hero -> hero.getName().equals(name))
                .map(this::fromHeroToHeroDto)
                //.map(hero -> new HeroDto(hero.getName(), heroMovieService.getPlayedIn(hero.getName())))
                .toList();
    }

    public HeroDto getHeroesByID(Long id) {
        return fromHeroToHeroDto(heroDao.findByID(id));
    }

    public HeroDto createHero(HeroDto heroDto) {
        heroDao.create(Hero.builder()
                        .name(heroDto.name())
                        .gender(heroDto.gender())
                        .build());
        return getHeroesByName(heroDto.name())
                .stream()
                //.map(this::fromHeroToHeroDto())
                .findFirst()
                .orElseThrow();
    }

    public HeroDto updateHero(HeroDto heroDto, long id) {
        heroDao.update(Hero.builder()
                .name(heroDto.name())
                .id(id)
                .gender(heroDto.gender())
                .build());
        return getHeroesByID(id);
    }

    public void deleteHero(Long id) {
        heroDao.delete(id);
    }

    private HeroDto fromHeroToHeroDto(Hero hero) {
        return findByName(hero.getId(), hero.getName(), hero.getGender());
    }

    private HeroDto findByName(Long id, String name, String gender) {
        return new HeroDto(id, name, gender, heroMovieService.getPlayedIn(name));
    }
}
