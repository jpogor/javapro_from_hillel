package ua.hillel.oop.hw25patterns;

import ua.hillel.oop.hw24jdbc.HeroDaoImpl;

import java.util.HashMap;
import java.util.List;

import static ua.hillel.oop.hw25patterns.HeroFabric.createDataSource;

public class Main {
    public static void main(String[] args) {
        HeroDto heroDto = new HeroDto.HeroDtoBuilder()
                .name("Hero_1")
                .movies(List.of("Film_1","Film_2"))
                .build();
        System.out.println("=== 1-2 " + "=".repeat(80));
        System.out.println("heroDto = " + heroDto);

        System.out.println("=== 3-3 " + "=".repeat(80));
        var heroMovies = new HashMap<String, List<String>>();
        heroMovies.put("Hero_01", List.of("Film_01", "Film_02", "Film_03"));
        heroMovies.put("Hero_02", List.of("Film_01", "Film_03", "Film_04"));
        HeroMovieService heroMovieService = new HeroMovieService(heroMovies);
        System.out.println("heroMovies = " + heroMovies);
        System.out.println("getPlayedIn('Hero_02') = " + heroMovieService.getPlayedIn("Hero_02"));

        System.out.println("=== 4-4 " + "=".repeat(80));
        var heroDao = new HeroDaoImpl(createDataSource());
        HeroService heroService = new HeroService(heroDao, heroMovieService);
        System.out.println("heroService = " + heroService);

        System.out.println("=== 5-5 " + "=".repeat(80));


    }
}
