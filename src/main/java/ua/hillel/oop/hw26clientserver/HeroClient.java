package ua.hillel.oop.hw26clientserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import static ua.hillel.oop.hw26clientserver.HeroProtocol.*;

public class HeroClient {

    public void execute() throws IOException {
        var scanner = new Scanner(System.in);

        var echoSocket = new Socket(SERVER_HOST, SERVER_PORT);
        var out = new PrintWriter(echoSocket.getOutputStream(), true);
        var in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));

        System.out.println("Client socket is ready. Type a message to send to the server: ");

        String userInput;
        while((userInput = scanner.nextLine()) != null) {
            out.println(userInput);
            if(userInput.equals(EXIT))
                break;
            System.out.println("Server: " + in.readLine());
        }
        in.close();
        out.close();
        echoSocket.close();
    }
}
