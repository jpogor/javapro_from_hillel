package ua.hillel.oop.hw26clientserver;

public class HeroProtocol {
    public static final String SERVER_HOST = "localhost";
    public static final int SERVER_PORT = 8085;
    public static final String EXIT = "-exit";
    public static final String FIND = "-name";
    public static final String ALL = "-all";
}
