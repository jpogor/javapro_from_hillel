package ua.hillel.oop.hw26clientserver;

import lombok.SneakyThrows;
import ua.hillel.oop.hw24jdbc.Hero;
import ua.hillel.oop.hw24jdbc.HeroDaoImpl;
import ua.hillel.oop.hw25patterns.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;

import static ua.hillel.oop.hw25patterns.HeroFabric.createDataSource;
import static ua.hillel.oop.hw26clientserver.HeroProtocol.*;

public class HeroServer {
    public static void main(String[] args) throws IOException {

            System.out.println("Connection successful");
            var serverSocket = new ServerSocket(SERVER_PORT);

            while(true) {
                System.out.println("Waiting for connection...");
                var clientSocket = serverSocket.accept();
                System.out.println("Connect success");
                new RequestHandler(clientSocket).start();
            }
            //serverSocket.close();
    }

    private static final class RequestHandler extends Thread {
        private final Socket clientSocket;

        private RequestHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @SneakyThrows
        public void run() {

            System.out.println("Thread name: " + Thread.currentThread().getName() + ". Waiting for input...");

            var out = new PrintWriter(clientSocket.getOutputStream(), true);
            var in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            HeroService heroService = new HeroService(new HeroDaoImpl(createDataSource()), new HeroMovieService(new HashMap<>()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Thread name: " + Thread.currentThread().getName() + ". Received: " + inputLine);

                if(inputLine.equals(EXIT))
                    break;

                if (inputLine.contains(FIND)) {
                    String heroName = inputLine.split(FIND)[1].trim();
                    List<Hero> result = heroService.heroDao().findByName(heroName);
                    if (result == null || result.isEmpty())
                        out.println("Hero " + heroName + " not found");
                    else
                        out.println(result);
                } else if (inputLine.equals(ALL)) {
                    List<Hero> result = heroService.heroDao().findAll();
                    if (result == null || result.isEmpty())
                        out.println("No heroes found");
                    else
                        out.println(result);
                } else {
                    out.println("Command [" + inputLine + "] is not found");
                }
                //out.println(outputLine);
            }
            out.close();
            in.close();
            clientSocket.close();
        }
    }
}
