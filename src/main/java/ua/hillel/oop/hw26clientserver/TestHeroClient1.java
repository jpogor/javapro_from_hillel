package ua.hillel.oop.hw26clientserver;

import java.io.IOException;

public class TestHeroClient1 {
    public static void main(String[] args) throws IOException {
        HeroClient heroClient = new HeroClient();
        heroClient.execute();
    }
}
