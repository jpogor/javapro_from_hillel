package ua.hillel.oop.hw28rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.hillel.oop.hw25patterns.HeroDto;
import ua.hillel.oop.hw25patterns.HeroService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class HeroRestController {
    private final HeroService heroService;

    @GetMapping("/heroes")
    public List<HeroDto> getHeroes(@RequestParam(value = "name", required = false) String heroName) {
        if (heroName == null)
            return heroService.getHeroes();
        return heroService.getHeroesByName(heroName);
    }
/*
    @GetMapping(params = "name")
    public List<Hero> getHero(
            @RequestParam(value = "name", required = false) String heroName,
            @RequestParam(value = "limit", required = false) Integer limit) {

        return heroService.getHeroesByName(heroName);
    }
*/
    @GetMapping("/heroes/{id}")
    public HeroDto getHeroesById(@PathVariable Long id) {
        return heroService.getHeroesByID(id);
    }

    @PostMapping("/heroes")
    public HeroDto createHero(@RequestBody HeroDto heroDto) {
        return heroService.createHero(heroDto);
    }

    @PutMapping("/heroes/{id}")
    public HeroDto updateHero(@PathVariable Long id, @RequestBody HeroDto heroDto) {
        return heroService.updateHero(heroDto, id);
    }

    @DeleteMapping("/heroes/{id}")
    public void deleteHeroes(@PathVariable Long id) {
        heroService.deleteHero(id);
    }
}
