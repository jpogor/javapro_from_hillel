package ua.hillel.oop.hw28rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ua.hillel.oop.hw24jdbc.HeroDaoImpl;
import ua.hillel.oop.hw25patterns.HeroMovieService;
import ua.hillel.oop.hw25patterns.HeroService;

import java.util.HashMap;

import static ua.hillel.oop.hw25patterns.HeroFabric.createDataSource;

@SpringBootApplication
public class RestApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
    }

    @Bean
    public HeroService heroService() {
        return new HeroService(new HeroDaoImpl(createDataSource()), new HeroMovieService(new HashMap<>()));
    }
}
