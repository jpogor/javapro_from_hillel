package ua.hillel.oop.hw29springcore;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ua.hillel.oop.hw24jdbc.*;
import ua.hillel.oop.hw25patterns.*;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@ComponentScan("ua.hillel.oop.hw29springcore")
@PropertySource("classpath:application.properties")

public class HeroConfiguration {

    @Value("${database}")
    private String database;
    @Value("${host}")
    private String host;
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @Bean
    public HeroService heroService(HeroDao heroDao, HeroMovieService heroMovieService) {
        return new HeroService(heroDao, heroMovieService);
    }

    @Bean
    public HeroDao heroDao(DataSource dataSource) {
        return new HeroDaoImpl(dataSource);
    }

    @Bean
    public DataSource dataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{host});
        dataSource.setDatabaseName(database);
        dataSource.setUser(login);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    public HeroMovieService heroMovieService() {
        return new HeroMovieService(new HashMap<>());
    }
}
