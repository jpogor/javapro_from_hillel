package ua.hillel.oop.hw29springcore;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.hillel.oop.hw25patterns.HeroService;

public class HeroMain {
    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(HeroConfiguration.class);
        var repository = context.getBean(HeroService.class);

        System.out.println("===== findAll() " + "=".repeat(80));
        System.out.println(repository.heroDao().findAll());

        System.out.println("===== findByName(\"Hero_01\") " + "=".repeat(80));
        System.out.println(repository.heroDao().findByName("Hero_01"));

        System.out.println("===== findByID(12L) " + "=".repeat(80));
        System.out.println(repository.heroDao().findByID(12L));

    }
}
