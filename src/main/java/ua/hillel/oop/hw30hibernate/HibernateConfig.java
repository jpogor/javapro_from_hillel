package ua.hillel.oop.hw30hibernate;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan("ua.hillel.oop.hw30hibernate")
@PropertySource("classpath:application.properties")

public class HibernateConfig {

    @Value("${database}")
    private String database;
    @Value("${host}")
    private String host;
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        var sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan("ua.hillel.oop.hw30hibernate");
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{host});
        dataSource.setDatabaseName(database);
        dataSource.setUser(login);
        dataSource.setPassword(password);
        return dataSource;
    }
}
