package ua.hillel.oop.hw30hibernate;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name="hero")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class HibernateHero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gender;
    private String eyeColor;
    private String race;
    private String hairColor;
    private double height;
    private String publisher;
    private String skinColor;
    private String alignment;
    private int weight;
    @Column(name="created_at")
    private LocalDateTime createdAt;
    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    public HibernateHero(HibernateHero hibernateHero) {
        this.id = hibernateHero.id;
        this.name = hibernateHero.name;
        this.gender = hibernateHero.gender;
        this.eyeColor = hibernateHero.eyeColor;
        this.race = hibernateHero.race;
        this.hairColor = hibernateHero.hairColor;
        this.height = hibernateHero.height;
        this.publisher = hibernateHero.publisher;
        this.skinColor = hibernateHero.skinColor;
        this.alignment = hibernateHero.alignment;
        this.weight = hibernateHero.weight;
        this.createdAt = hibernateHero.createdAt;
        this.updatedAt = hibernateHero.updatedAt;
    }
}
