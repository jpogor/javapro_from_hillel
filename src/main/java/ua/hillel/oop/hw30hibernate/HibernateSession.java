package ua.hillel.oop.hw30hibernate;

import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;

import java.util.List;

public record HibernateSession(Session session) {

    public HibernateHero findByID(Long id) {
        return session.find(HibernateHero.class, id);
    }

    public List<HibernateHero> findAll() {
        //return session.createQuery("SELECT h FROM hero h", HibernateHero.class).getResultList();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<HibernateHero> cq = cb.createQuery(HibernateHero.class);
        Root<HibernateHero> rootEntry = cq.from(HibernateHero.class);
        CriteriaQuery<HibernateHero> all = cq.select(rootEntry);

        TypedQuery<HibernateHero> allQuery = session.createQuery(all);
        return allQuery.getResultList();
    }

/*
    public HibernateHero add(HibernateHero hibernateHero) {
        var id = session.save(hibernateHero);
        return session.find(HibernateHero.class, id);
    }
*/
    public HibernateHero add(HibernateHero hibernateHero) {
        session.persist(hibernateHero);
        return session.find(HibernateHero.class, hibernateHero.getId());
    }

    public HibernateHero update(HibernateHero hibernateHero) {
        return session.merge(hibernateHero);
    }

    public HibernateHero delete(HibernateHero hibernateHero) {
        HibernateHero hero = new HibernateHero(hibernateHero);
        session.remove(hibernateHero);
        return hero;
    }
}
