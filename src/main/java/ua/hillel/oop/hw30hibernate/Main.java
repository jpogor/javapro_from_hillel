package ua.hillel.oop.hw30hibernate;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j

public class Main {
    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(HibernateConfig.class);
        var sessionFactory = context.getBean(SessionFactory.class);

        try (Session session = sessionFactory.openSession()) {
            System.out.println("=== find " + "=".repeat(80));
            var hero = session.find(HibernateHero.class, 10L);
            System.out.println("Found hero = " + hero);
            log.info("Found hero = " + hero);
            System.out.println("=".repeat(90));

            var transaction = session.beginTransaction();
            var hibernateSession = new HibernateSession(session);
            var addedHero = hibernateSession.add(HibernateHero.builder()
                    .name("Hero_" + UUID.randomUUID().toString().substring(0,3))
                    .gender("Female")
                    .eyeColor("Red")
                    .race("Brown")
                    .hairColor("Blue")
                    .height(155.5)
                    .publisher("Elephant")
                    .skinColor("White")
                    .alignment("Evil")
                    .weight(88)
                    .createdAt(LocalDateTime.now())
                    .updatedAt(LocalDateTime.now())
                    .build());
            System.out.println("=== addedHero = " + addedHero);
            System.out.println("=".repeat(90));
            transaction.commit();

            transaction = session.beginTransaction();
            var heroForUpdated = HibernateHero.builder()
                    .id(addedHero.getId())
                    .name(addedHero.getName() + UUID.randomUUID().toString().substring(0,3))
                    .gender(addedHero.getGender())
                    .eyeColor(addedHero.getEyeColor())
                    .race(addedHero.getRace())
                    .hairColor(addedHero.getHairColor())
                    .height(222)
                    .publisher(addedHero.getPublisher())
                    .updatedAt(LocalDateTime.now())
                    .build();
            var updatedHero = hibernateSession.update(heroForUpdated);
            System.out.println("=== updatedHero = " + updatedHero);
            System.out.println("=".repeat(90));
            transaction.commit();

            transaction = session.beginTransaction();
            //var deletedHero = hibernateSession.delete(updatedHero);
            var deletedHero = hibernateSession.findByID(348L);
            hibernateSession.delete(deletedHero);
            System.out.println("=== deletedHero = " + deletedHero);
            System.out.println("=".repeat(90));
            transaction.commit();

            System.out.println("=== AllHeroes = " +  hibernateSession.findAll());
            System.out.println("=".repeat(90));
        }
    }
}
