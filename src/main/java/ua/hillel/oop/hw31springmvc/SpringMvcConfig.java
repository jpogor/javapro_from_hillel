package ua.hillel.oop.hw31springmvc;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ua.hillel.oop.hw24jdbc.HeroDaoImpl;
import ua.hillel.oop.hw25patterns.HeroMovieService;
import ua.hillel.oop.hw25patterns.HeroService;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@ComponentScan("ua.hillel.oop.hw31springmvc")
@PropertySource("classpath:application.properties")
public class SpringMvcConfig {

    @Value("${database}")
    private String database;
    @Value("${host}")
    private String host;
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @Bean
    public HeroService heroService() {
        return new HeroService(new HeroDaoImpl(setDataSource()), new HeroMovieService(new HashMap<>()));
    }
    @Bean
    public DataSource setDataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{host});
        dataSource.setDatabaseName(database);
        dataSource.setUser(login);
        dataSource.setPassword(password);
        return dataSource;
    }
}
