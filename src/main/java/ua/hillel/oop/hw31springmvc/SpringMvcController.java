package ua.hillel.oop.hw31springmvc;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.hillel.oop.hw25patterns.HeroDto;
import ua.hillel.oop.hw25patterns.HeroService;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class SpringMvcController {
    private final HeroService heroService;

    @GetMapping
    public String heroes(Model model) {
        model.addAttribute("name", "SpringMvcController");
        model.addAttribute("heroes", heroService.getHeroes());
        return "heroes/index";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("hero", new HeroDto.HeroDtoBuilder()
                    .name("Hero_New")
                    .gender("Female")
                    .build());
        return "heroes/create";
    }

    @PostMapping("/add")
    public String create(HeroDto heroDto, Model model) {
        heroService.createHero(heroDto);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        var heroDto = heroService.getHeroesByID(id); //.orElseThrow();
        model.addAttribute("hero", heroDto);
        return "heroes/edit";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") long id, HeroDto heroDto, Model model) {
        heroService.updateHero(heroDto, id);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        heroService.deleteHero(id);
        return "redirect:/";
    }
}
