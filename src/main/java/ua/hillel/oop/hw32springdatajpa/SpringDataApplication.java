package ua.hillel.oop.hw32springdatajpa;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import ua.hillel.oop.hw32springdatajpa.hero.SpringDataHeroRepository;
import ua.hillel.oop.hw32springdatajpa.user.SpringUserService;
import ua.hillel.oop.hw32springdatajpa.user.UserDto;
import ua.hillel.oop.hw32springdatajpa.user.UserRole;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
@EnableJpaAuditing
public class SpringDataApplication implements CommandLineRunner {

    private final SpringDataHeroRepository heroRepository;
    private final SpringUserService springUserService;
    public static void main(String[] args) {
        SpringApplication.run(SpringDataApplication.class, args);
    }
    @Override
    public void run(String... args) throws Exception {
        log.info("Heroes: {}", heroRepository.findAll());

        //SpringUserService springUserService = new SpringUserService(this.springUserService);
        log.info("Users: {}", this.springUserService.findAll());
        UserDto user = UserDto.builder()
                //.uid(UUID.randomUUID().toString())
                .name("User_01")
                .email("user01@examle.com")
                .userRole(UserRole.CUSTOMER)
                //.createdAt(LocalDateTime.now())
                //.updatedAt(LocalDateTime.now())
                .build();
        user = springUserService.add(user);
        log.info("New user: {}", user);
        log.info("Users: FindById(newUser): {}", springUserService.findById(user.id()));
        log.info("Users: FindByName(User_01): {}", springUserService.findByName("User_01"));
        //log.info("Users: FindByNameLike(User*): {}", this.springUserService.findByNameLike("User%"));
        //log.info("Users: FindByEmail(): {}", springUserService.findByEmail("admin01@test.com"));
        //log.info("Users: FindByNameAndUserrole(): {}", usersRepository.findByNameAndUserrole("User_01", UserRole.ADMIN));

        log.info("=".repeat(90));
        user = UserDto.builder()
                .name("Admin_01")
                .email("admin01@test.com")
                .userRole(UserRole.ADMIN)
                .build();
        user = springUserService.add(user);
        log.info("add = " + user);
        user = springUserService.findById(1L);
        user = springUserService.update(UserDto.builder()
                        .id(user.id())
                        .uid(user.uid())
                        .name("User_11")
                        .email("new-mail@test.com")
                        .userRole(UserRole.CUSTOMER)
                        .createdAt(user.createdAt())
                        .updatedAt(user.updatedAt())
                        .build());
        log.info("update = " + user);
        user = springUserService.findById(2L);
        log.info("delete = " + user);
        springUserService.delete(user);
        log.info("=".repeat(90));
    }
}
