package ua.hillel.oop.hw32springdatajpa.hero;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name="hero")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SpringDataHero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gender;
    private String eyecolor;
    private String race;
    private String haircolor;
    private double height;
    private String publisher;
    private String skincolor;
    private String alignment;
    private int weight;
    //@Column(name="created_at")
    private LocalDateTime createdAt;
    //@Column(name="updated_at")
    private LocalDateTime updatedAt;
}
