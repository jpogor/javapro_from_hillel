package ua.hillel.oop.hw32springdatajpa.hero;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataHeroRepository extends JpaRepository<SpringDataHero, Long> {

}
