package ua.hillel.oop.hw32springdatajpa.user;

import jakarta.persistence.*;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.data.annotation.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.lang.annotation.*;
import java.time.LocalDateTime;
import java.util.UUID;

import static java.time.LocalDateTime.now;

@Data
@Entity
@Table(name="users")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
public class SpringDataUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, name = "uid", nullable = false)
    private String uid; // = UUID.randomUUID().toString();
    private String name;
    private String email;
    @Enumerated(EnumType.STRING)
    private UserRole userrole;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime updatedAt;

    @PrePersist
    public void autofill() {
        uid = UUID.randomUUID().toString();
//      createdAt = now();
//      updatedAt = now();
    }
}
