package ua.hillel.oop.hw32springdatajpa.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SpringDataUserRepository extends JpaRepository<SpringDataUser, Long> {
    Optional<SpringDataUser> findById(Long id);
    Optional<SpringDataUser> findByUid(String uid);
    List<SpringDataUser> findByName(String name);
    Optional<SpringDataUser> findByEmail(String email);
    List<SpringDataUser> findByNameLike(String name);

    List<SpringDataUser> findByNameAndUserrole(String name, UserRole userRole);
}
