package ua.hillel.oop.hw32springdatajpa.user;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class SpringUserService {

    private final SpringDataUserRepository userRepository;

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(this::fromEntityToDto)
                //.map(hero -> new HeroDto(hero.getId(), hero.getName(), hero.getGender(), heroMovieService.getPlayedIn(hero.getName())))
                .toList();
    }
    public UserDto findById(Long id) {
        return userRepository.findById(id)
                .map(this::fromEntityToDto)
                .orElse(null);
    }
    public UserDto findByUid(String uid) {
        return userRepository.findByUid(uid)
                .map(this::fromEntityToDto)
                .orElse(null); //.orElseThrow();
    }
    public UserDto getUserByUid(String uid) {
        return userRepository.findByUid(uid)
                .map(this::fromEntityToDto)
                .orElse(null); //.orElseThrow();
    }
    public List<UserDto> findByEmail(String email) {
        return userRepository.findByEmail(email).stream()
                .map(this::fromEntityToDto)
                .toList();
    }
    public List<UserDto> findByName(String name) {
        return userRepository.findByName(name).stream()
                .map(this::fromEntityToDto)
                .toList();
    }
    public UserDto add(UserDto UserDto) {
        return fromEntityToDto(userRepository.save(SpringDataUser.builder()
                        .name(UserDto.name())
                        //.uid(UUID.randomUUID().toString())
                        .name(UserDto.name())
                        .email(UserDto.email())
                        .userrole(UserDto.userRole())
                        //.createdAt(LocalDateTime.now())
                        //.updatedAt(LocalDateTime.now())
                        .build()));
    }
    public UserDto update(UserDto userDto) {
        var user = userRepository.findById(userDto.id()).orElse(null);
        return fromEntityToDto(userRepository.save(SpringDataUser.builder()
                        .id(user.getId())
                        .uid(user.getUid())
                        .name(userDto.name())
                        .email(userDto.email())
                        .userrole(userDto.userRole())
                        .updatedAt(LocalDateTime.now())
                        .build()));
    }
    public void delete(UserDto userDto) {
        if(userDto != null) // мне не нужно отваливаться в exeption при удалении null
            userRepository.delete(userRepository.findById(userDto.id()).orElse(null));
    }

    private UserDto fromEntityToDto(SpringDataUser user) {
        return UserDto.builder()
                .id(user.getId())
                .uid(user.getUid())
                .name(user.getName())
                .email(user.getEmail())
                .userRole(user.getUserrole())
                .createdAt(user.getCreatedAt())
                .updatedAt(user.getUpdatedAt())
                .build();
    }
}
