package ua.hillel.oop.hw32springdatajpa.user;

import lombok.Builder;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
public record UserDto(Long id, String uid, String name, String email, UserRole userRole, LocalDateTime createdAt, LocalDateTime updatedAt) {
}
