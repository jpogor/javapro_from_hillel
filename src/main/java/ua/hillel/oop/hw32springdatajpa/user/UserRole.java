package ua.hillel.oop.hw32springdatajpa.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
@ToString
public enum UserRole {
    ADMIN("Админ"),
    CUSTOMER("Юзер");

    private final String role;
}
