package ua.hillel.oop.hw33bank;

import org.apache.logging.log4j.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BankApplication {
    private static final Logger logger = LogManager.getLogger(BankApplication.class);

    public static void main(String[] args) {
        Exception e = new RuntimeException("This is only a test!");
        logger.info("Bank Application Start (info)...");
        logger.debug("Bank Application Start (debug)...");
        logger.error("Bank Application Start (error)...", e);
        SpringApplication.run(BankApplication.class);
    }
}
