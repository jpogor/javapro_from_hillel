/* congig не нужен, если правильно прописать параметры в application.properties
package ua.hillel.oop.hw33bank;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@ComponentScan("ua.hillel.oop.hw33bank")
@PropertySource("classpath:application.properties")
public class BankConfig {

    @Value("${database}")
    private String database;
    @Value("${host}")
    private String host;
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @Bean
    public DataSource dataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{host});
        dataSource.setDatabaseName(database);
        dataSource.setUser(login);
        dataSource.setPassword(password);
        return dataSource;
    }
}
*/
