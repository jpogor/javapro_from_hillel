package ua.hillel.oop.hw33bank.account;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ua.hillel.oop.hw33bank.entity.BaseEntity;
import ua.hillel.oop.hw33bank.person.Person;
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "accounts")
@EntityListeners(AuditingEntityListener.class)
public class Account extends BaseEntity {
    private String uid;
    private String iban;
    private Long balance;
    //private Long personId;
    @ManyToOne
    private Person person;
}
