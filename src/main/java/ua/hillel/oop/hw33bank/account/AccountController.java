package ua.hillel.oop.hw33bank.account;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public List<AccountDto> getAll(
            @RequestParam(value = "iban", required = false) String iban,
            @RequestParam(value = "balance", required = false) Long balance,
            Pageable pageable) {
        log.info("getAccounts: iban={}, balance={}", iban, balance);
        return accountService.findAccounts(iban, balance, pageable);
    }

    @GetMapping("/{id}")
    public AccountDto account(@PathVariable("id") String id) {
        return accountService.getAccount(id);
    }

    @PostMapping
    public AccountDto createAccount(@RequestBody AccountDto request) {
        return accountService.create(request);
    }

    @PutMapping("/{id}")
    public AccountDto updatedAccount(
            @PathVariable("id") String id,
            @RequestBody AccountDto request) {
        return accountService.update(request, id);
    }

    @DeleteMapping("/{id}")
    public void deleteAccount(@PathVariable("id") String id) {
        accountService.delete(id);
    }
}
