package ua.hillel.oop.hw33bank.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import ua.hillel.oop.hw33bank.person.PersonDto;

public record AccountDto(
        String id,
        String iban,
        Long balance,
        @JsonProperty("personId")
        String personId,
        PersonDto person
) {
}