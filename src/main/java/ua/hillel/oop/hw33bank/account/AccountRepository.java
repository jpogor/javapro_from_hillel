package ua.hillel.oop.hw33bank.account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ua.hillel.oop.hw33bank.person.Person;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {

    Optional<Account> findByUid(String id);
    Page<Account> findByIban(String iban, Pageable pageable);
    Page<Account> findByBalance(Long balance, Pageable pageable);
    Page<Account> findByPerson(Person person, Pageable pageable);
    Page<Account> findByIbanAndBalance(String iban,Long balance, Pageable pageable);
}