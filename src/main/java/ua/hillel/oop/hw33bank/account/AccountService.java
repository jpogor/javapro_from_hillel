package ua.hillel.oop.hw33bank.account;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.hillel.oop.hw33bank.person.Person;
import ua.hillel.oop.hw33bank.person.PersonDto;
import ua.hillel.oop.hw33bank.person.PersonRepository;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final PersonRepository personRepository;

    public List<AccountDto> findAccounts(String iban, Long balance, Pageable pageable) {
        var specification = Specification.where(fieldValueEqualQuery("iban", iban))
                                                               .and(fieldValueEqualQuery("balance", balance));
        return accountRepository.findAll(specification).stream()
                .map(this::mapAccount)
                .toList();
    }

    private Specification<Account> fieldValueEqualQuery(String fieldName, Object fieldValue) {
        return (fieldValue == null) ? null : (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(fieldName), fieldValue);
    }

    public AccountDto getAccount(String id) {
        return accountRepository.findByUid(id)
                .map(this::mapAccount)
                .orElseThrow();
    }

    @Transactional
    public AccountDto create(AccountDto request) {
        var person = personRepository.findByUid(request.personId()).orElse(null);
        return mapAccount(accountRepository.save(Account.builder()
                .uid(UUID.randomUUID().toString())
                .iban(request.iban())
                .balance(request.balance())
                .person(person)
                .build()));
    }

    @Transactional
    public AccountDto update(AccountDto request, String uid) {
        var account = accountRepository.findByUid(uid).orElse(null);
        account.setIban(request.iban() == null ? account.getIban() : request.iban());
        account.setBalance(request.balance() == null ? account.getBalance() : request.balance());
        //account.setPerson(request.person() == null ? account.getPerson() : request.person());
        return mapAccount(accountRepository.save(account));
    }

    @Transactional
    public void delete(String id) {
        accountRepository.delete(accountRepository.findByUid(id).orElse(null));
    }

    private AccountDto mapAccount(Account account) {
        return new AccountDto(account.getUid(), account.getIban(), account.getBalance(), account.getPerson().getUid(), mapPerson(account.getPerson()));
    }

    private PersonDto mapPerson(Person person) {
        return new PersonDto(person.getUid(), person.getName(), person.getEmail(), List.of());
    }
}
