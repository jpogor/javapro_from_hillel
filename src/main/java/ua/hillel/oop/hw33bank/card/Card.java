package ua.hillel.oop.hw33bank.card;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.entity.BaseEntity;
import ua.hillel.oop.hw33bank.person.Person;

import java.time.Instant;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cards")
@EntityListeners(AuditingEntityListener.class)
public class Card extends BaseEntity {
    private String uid;
    private String pan;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
    short pin;
    short cvv;
    @Enumerated(EnumType.STRING)
    private CardStatus status;
    Instant expirationDate;
}
