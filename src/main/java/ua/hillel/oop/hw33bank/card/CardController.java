package ua.hillel.oop.hw33bank.card;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/cards")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @GetMapping
    public List<CardDto> getAll(
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "pan", required = false) String pan,
            Pageable pageable) {
        log.info("getCards: uid={}, pan={}", uid, pan);
        return cardService.findCard(uid, pan, pageable);
    }

    @GetMapping("/{id}")
    public CardDto account(@PathVariable("id") String id) {
        return cardService.getCard(id);
    }

    @PostMapping
    public CardDto createCard(@RequestBody CardDto request) {
        return cardService.openCard(request);
    }

    @PutMapping("/{id}")
    public CardDto updateCard(
            @PathVariable("id") String id,
            @RequestBody CardDto request) {
        return cardService.updateCard(request, id);
    }

    @DeleteMapping("/{id}")
    public void deleteCard(@PathVariable("id") String id) {
        cardService.deleteCard(id);
    }
}
