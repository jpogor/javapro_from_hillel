package ua.hillel.oop.hw33bank.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;

public record CardDto(
        String id,
        String pan,
        @JsonProperty("account_id")
        String accountId,
//      AccountDto account,
        @JsonProperty("person_id")
        String personId,
//      PersonDto person,
        short pin,
        short cvv,
        CardStatus status,
        @JsonProperty("expiration_date")
        Instant expirationDate
) {
}