package ua.hillel.oop.hw33bank.card;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.hillel.oop.hw33bank.account.AccountRepository;
import ua.hillel.oop.hw33bank.person.PersonRepository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CardService {
    private final CardRepository cardRepository;
    private final AccountRepository accountRepository;
    private final PersonRepository personRepository;

    public List<CardDto> findCard(String uid, String pan, Pageable pageable) {
        var specification = Specification.where(fieldValueEqualQuery("uid", uid))
                .and(fieldValueEqualQuery("pan", pan));
        return cardRepository.findAll(specification).stream()
                .map(this::mapCard)
                .toList();
    }

    private Specification<Card> fieldValueEqualQuery(String fieldName, Object fieldValue) {
        return (fieldValue == null) ? null : (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(fieldName), fieldValue);
    }

    public CardDto getCard(String id) {
        return cardRepository.findByUid(id)
                .map(this::mapCard)
                .orElseThrow();
    }

    @Transactional
    public CardDto openCard(CardDto request) {
        if (cardRepository.existsByPan(request.pan())) {
            throw new RuntimeException("PAN [" + request.pan() + "] already exist");
        }
        var account =  accountRepository.findByUid(request.accountId())
                .orElseThrow(() -> new RuntimeException("Account [" + request.accountId() + "] not found"));
        var person = personRepository.findByUid(request.personId())
                .orElse(personRepository.findByUid(account.getPerson().getUid())
                        .orElseThrow(() -> new RuntimeException("Person [" + request.personId() + "] not found")));
        var pan = (request.pan() == null) ? "7777" + (long) (Math.floor(Math.random() * 900000000000L) + 100000000000L) : request.pan();
        var pin = (request.pin() == 0) ? (short) (Math.floor(Math.random() * 9000) + 1000) : request.pin();
        var cvv = (request.cvv() == 0) ? (short) (Math.floor(Math.random() * 900) + 100) : request.cvv();
        var expirationDate = (request.expirationDate() != null) ? request.expirationDate() : Instant.now().plus(364, ChronoUnit.DAYS);
        return mapCard(cardRepository.save(Card.builder()
                .uid(UUID.randomUUID().toString())
                .pan(pan)
                .account(account)
                .person(person)
                .pin(pin)
                .cvv(cvv)
                .status(request.status())
                .expirationDate(expirationDate)
                .build()));
    }

    @Transactional
    public CardDto updateCard(CardDto request, String uid) {
        var card = cardRepository.findByUid(uid)
                .orElseThrow(() -> new RuntimeException("Card [" + uid + "] not found"));
        card.setPan((request.pan() == null) ? card.getPan() : request.pan());
        var account = accountRepository.findByUid(request.accountId()).orElseThrow();
        card.setAccount((request.accountId() == null) ? card.getAccount() : account);
        var person = personRepository.findByUid(request.personId()).orElseThrow();
        card.setPerson((request.personId() == null) ? card.getPerson() : person);
        card.setPin((request.pin() <= 0) ? card.getPin() : request.pin());
        card.setCvv((request.cvv() <= 0) ? card.getCvv() : request.cvv());
        card.setStatus((request.status() == null) ? card.getStatus() : request.status());
        card.setExpirationDate((request.expirationDate() == null) ? card.getExpirationDate() : request.expirationDate());
        return mapCard(cardRepository.save(card));
    }

    @Transactional
    public void deleteCard(String id) {
        cardRepository.delete(cardRepository.findByUid(id).orElse(null));
    }

    private CardDto mapCard(Card card) {
        return new CardDto(card.getUid(), card.getPan(), card.getAccount().getUid(), card.getPerson().getUid(),
                           card.getPin(), card.getCvv(), card.getStatus(), card.getExpirationDate());
    }
}
