package ua.hillel.oop.hw33bank.card;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
@ToString
public enum CardStatus {
    ACTIVE(1,"ACTIVE"),
    CLOSED(2,"CLOSED");

    private final Integer status;
    private final String cardStatus;

}
