package ua.hillel.oop.hw33bank.currency.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.hillel.oop.hw33bank.currency.converter.CurrencyApiCurrencyConverter;
import ua.hillel.oop.hw33bank.currency.converter.CurrencyConverter;
import ua.hillel.oop.hw33bank.currency.converter.CurrencyConverterProperties;
import ua.hillel.oop.hw33bank.currency.converter.DummyCurrencyConverter;

@Configuration
public class CurrencyConfig {
    @Configuration
    @ConditionalOnProperty(name = "currency.provider", havingValue = "currencyapi")
    public static class CurrencyProviderConfiguration {
        @Bean
        public CurrencyConverter currencyConverter(CurrencyConverterProperties currencyConverterProperties) {
            return new CurrencyApiCurrencyConverter(currencyConverterProperties);
        }
    }

    @Configuration
    @ConditionalOnProperty(name = "currency.provider", havingValue = "dummy")
    public static class DummyCurrencyProviderConfiguration {
        @Bean
        public CurrencyConverter currencyConverter() {
            return new DummyCurrencyConverter();
        }
    }
}
