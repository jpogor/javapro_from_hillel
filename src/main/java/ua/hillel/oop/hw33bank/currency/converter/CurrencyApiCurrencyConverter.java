package ua.hillel.oop.hw33bank.currency.converter;

import org.springframework.web.reactive.function.client.WebClient;
import ua.hillel.oop.hw33bank.currency.model.CurrencyResponse;

import java.util.Currency;

public class CurrencyApiCurrencyConverter implements CurrencyConverter {

    private final WebClient webClient;
    private final CurrencyConverterProperties currencyConverterProperties;

    public CurrencyApiCurrencyConverter(CurrencyConverterProperties currencyConverterProperties) {
        this.currencyConverterProperties = currencyConverterProperties;
        webClient = WebClient.builder().baseUrl(currencyConverterProperties.getUrl()).build();
    }
    @Override
    public double convert(Currency from, Currency to, double amount) throws NullPointerException {
        System.setProperty ("javax.net.ssl.trustStore", "NUL");                 // Отключить проверку SSL сертификатов
        System.setProperty ("javax.net.ssl.trustStoreType", "Windows-ROOT");    // Отключить проверку SSL сертификатов
        var response = webClient.get()
                .uri(uri -> uri.path("/v3/latest")
                        .queryParam("base_currency", from.getCurrencyCode())
                        .queryParam("currencies", to.getCurrencyCode())
                        .queryParam("apikey", currencyConverterProperties.getApiKey())
                        .build())
                .retrieve()
                .bodyToMono(CurrencyResponse.class)
                .block()
                .getData();
        return amount * response.get(to.getCurrencyCode()).getValue();
    }
}
