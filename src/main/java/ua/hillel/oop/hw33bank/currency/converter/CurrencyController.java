package ua.hillel.oop.hw33bank.currency.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.hillel.oop.hw33bank.person.PersonOperationsService;

import java.util.Currency;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/currency")
@RequiredArgsConstructor
public class CurrencyController {
    private final CurrencyConverter currencyConverter;

    @GetMapping("/convert")
    @ResponseBody
    public double convert(@RequestParam Currency from, @RequestParam Currency to, @RequestParam double amount) {
        return currencyConverter.convert(from, to, amount);
    }

    private final PersonOperationsService personOperationsService;

    @GetMapping("/convert_v2")
    @ResponseBody
    public double convert_v2(@RequestParam Currency from, @RequestParam Currency to, @RequestParam double amount) throws ExecutionException, InterruptedException {
        return personOperationsService.convert(from, to, amount);
    }

}
