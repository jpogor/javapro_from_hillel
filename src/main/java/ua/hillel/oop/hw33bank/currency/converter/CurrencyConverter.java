package ua.hillel.oop.hw33bank.currency.converter;

import java.util.Currency;

public interface CurrencyConverter {
    public double convert(Currency from, Currency to, double amount);
}
