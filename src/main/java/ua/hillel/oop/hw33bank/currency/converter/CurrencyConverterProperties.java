package ua.hillel.oop.hw33bank.currency.converter;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@RequiredArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "currency")
public class CurrencyConverterProperties {
    private String url;
    private String apiKey;
}
