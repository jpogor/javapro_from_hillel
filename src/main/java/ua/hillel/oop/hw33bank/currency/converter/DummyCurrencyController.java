package ua.hillel.oop.hw33bank.currency.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Currency;

@RestController
@RequestMapping("/squidex")
@RequiredArgsConstructor
public class DummyCurrencyController {
    private final CurrencyConverter currencyConverter;

    @GetMapping("/convert")
    @ResponseBody
    public double convert(@RequestParam Currency from, @RequestParam Currency to, @RequestParam double amount) {
        return currencyConverter.convert(from, to, amount);
    }
}
