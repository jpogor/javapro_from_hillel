package ua.hillel.oop.hw33bank.currency.model;

import lombok.*;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyResponse {
    private Map<String, CurrencyResponseData> data;
    //private ResponseData data;
}
