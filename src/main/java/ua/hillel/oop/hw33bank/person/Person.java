package ua.hillel.oop.hw33bank.person;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.entity.BaseEntity;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "persons")
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode(callSuper=false)
public class Person extends BaseEntity {
    private String uid;
    private String name;
    private String email;
    @OneToMany(mappedBy = "person")
    private List<Account> accounts;
}