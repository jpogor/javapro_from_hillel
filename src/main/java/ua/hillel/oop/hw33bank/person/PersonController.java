package ua.hillel.oop.hw33bank.person;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/persons")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping("/all")
    public List<PersonDto> getAll(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            Pageable pageable) {
        log.info("getPersons: name={}, email={}", name, email);
        return personService.findPersons(name, email, pageable);
    }

    @GetMapping
    public List<PersonDto> findAll(Pageable pageable) {
        return personService.findAll(pageable);
    }
    @GetMapping("/{id}")
    public PersonDto person(@PathVariable("id") String id) {
        return personService.getPerson(id);
    }

    @PostMapping
    public PersonDto createPerson(@RequestBody PersonDto request) {
        return personService.create(request);
    }

    @PutMapping("/{id}")
    public PersonDto updatedPerson(
            @PathVariable("id") String id,
            @RequestBody PersonDto request) {
        return personService.update(request, id);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable("id") String id) {
        personService.delete(id);
    }
}