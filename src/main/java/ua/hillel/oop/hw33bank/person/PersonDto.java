package ua.hillel.oop.hw33bank.person;

import ua.hillel.oop.hw33bank.account.AccountDto;

import java.util.List;

public record PersonDto(
        String id,
        String name,
        String email,
        List<AccountDto> accounts
) {
}