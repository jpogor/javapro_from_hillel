package ua.hillel.oop.hw33bank.person;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import ua.hillel.oop.hw33bank.currency.converter.CurrencyConverter;

import java.util.Currency;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonOperationsService {
    private final CurrencyConverter currencyConverter;

    public double convert(Currency from, Currency to, double amount) throws ExecutionException, InterruptedException {
        MDC.put("Currency from", from.getCurrencyCode());
        MDC.put("Currency to", to.getCurrencyCode());
        MDC.put("Amount", String.valueOf(amount));

        CompletableFuture<Double> completableFuture = CompletableFuture.supplyAsync(() -> currencyConverter.convert(from, to, amount));
        return completableFuture.get();
    }
}
