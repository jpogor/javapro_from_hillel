package ua.hillel.oop.hw33bank.person;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.hillel.oop.hw33bank.account.AccountDto;
import ua.hillel.oop.hw33bank.account.Account;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public List<PersonDto> findPersons(String name, String email, Pageable pageable) {
        var specification = Specification.where(fieldValueEqualQuery("name", name))
                .and(fieldValueEqualQuery("email", email));
        return personRepository.findAll(specification).stream()
                .map(this::mapPerson)
                .toList();
    }

    private Specification<Person> fieldValueEqualQuery(String fieldName, Object fieldValue) {
        return (fieldValue == null) ? null : (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(fieldName), fieldValue);
    }

    public PersonDto getPerson(String id) {
        return personRepository.findByUid(id)
                .map(this::mapPerson)
                .orElseThrow();
    }

    public List<PersonDto> findAll(Pageable pageable) {
        return personRepository.findAll().stream()
                .map(this::mapPerson)
                .toList();
    }

    public PersonDto findByUid(String uid) {
        return personRepository.findByUid(uid)
                .map(this::mapPerson)
                .orElse(null); //.orElseThrow();
    }

    @Transactional
    public PersonDto create(PersonDto request) {
        return mapPerson(personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name(request.name())
                .email(request.email())
                .accounts(List.of())
                .build()));
    }

    @Transactional
    public PersonDto update(PersonDto request, String id) {
        var person = personRepository.findByUid(id).orElse(null);
        person.setName(request.name() == null ? person.getName() : request.name());
        person.setEmail(request.email() == null ? person.getEmail() : request.email());
        return mapPerson(personRepository.save(person));
    }

    @Transactional
    public void delete(String id) {
        personRepository.delete(personRepository.findByUid(id).orElseThrow());
    }

    private PersonDto mapPerson(Person person) {
        return new PersonDto(person.getUid(), person.getName(), person.getEmail(), person.getAccounts().stream()
                                                                                    .map(this::mapAccount)
                                                                                    .toList());
    }
    private AccountDto mapAccount(Account account) {
        return new AccountDto(account.getUid(), account.getIban(), account.getBalance(), null, null);
    }
}
