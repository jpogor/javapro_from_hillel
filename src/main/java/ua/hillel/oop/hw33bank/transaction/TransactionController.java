package ua.hillel.oop.hw33bank.transaction;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/transactions")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;

    @GetMapping
    public List<TransactionDto> getAll(
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
//            @RequestParam(value = "amount",required = false) Long amount,
            Pageable pageable) {
        log.info("getAll: from={}, to={}", from, to);
        return transactionService.findTransaction(from, to, pageable);
    }

    @GetMapping("/{id}")
    public TransactionDto transaction(@PathVariable("id") String id) {
        return transactionService.getTransaction(id);
    }

    @PostMapping
    public ResponseEntity<?> /*TransactionDto*/ newTransaction(@RequestBody TransactionDto request) {
        try {
            var dto = transactionService.newTransaction(request);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (Exception e) {
            var result = new HashMap<String, String>();
            result.put("errorCode", String.valueOf(HttpStatus.NOT_FOUND));
            result.put("errorText", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
        //return transactionService.newTransaction(request);
    }
/*
    @PutMapping("/{id}")
    public TransactionDto updateTransaction(
            @PathVariable("id") String id,
            @RequestBody TransactionDto request) {
        return transactionService.updateTransaction(request, id);
    }

    @DeleteMapping("/{id}")
    public void deleteTransaction(@PathVariable("id") String id) {
        transactionService.deleteTransaction(id);
    }
 */
}
