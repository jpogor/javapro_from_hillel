package ua.hillel.oop.hw33bank.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
public record TransactionDto(
        String id,
        @JsonProperty("from")
        String fromCardId,
        @JsonProperty("to")
        String toCardId,
        Long amount,
        Integer errCode,
        String errMessage
) {
        public TransactionDto(String id, String fromCardId, String toCardId, Long amount) {
                this(id, fromCardId, toCardId, amount, 200, "Ok");
        }
}