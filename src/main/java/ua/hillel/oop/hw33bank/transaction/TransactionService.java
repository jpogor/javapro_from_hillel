package ua.hillel.oop.hw33bank.transaction;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ua.hillel.oop.hw33bank.account.AccountRepository;
import ua.hillel.oop.hw33bank.card.CardRepository;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final AccountRepository accountRepository;
    private final CardRepository cardRepository;
    private final TransactionRepository transactionRepository;

    private TransactionDto mapTransaction(Transaction transaction) {
        return new TransactionDto(transaction.getUid(), transaction.getFromCardId(), transaction.getToCardId(), transaction.getAmount());
    }

    public List<TransactionDto> findTransaction(String from, String to, Pageable pageable) {
        var specification = Specification.where(fieldValueEqualQuery("from", from))
                .and(fieldValueEqualQuery("to", to));
        return transactionRepository.findAll(specification).stream()
                .map(this::mapTransaction)
                .toList();
    }

    private Specification<Transaction> fieldValueEqualQuery(String fieldName, Object fieldValue) {
        return (fieldValue == null) ? null : (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(fieldName), fieldValue);
    }

    public TransactionDto getTransaction(String id) {
        return transactionRepository.findByUid(id)
                .map(this::mapTransaction)
                .orElseThrow();
    }

    @Transactional
    public TransactionDto newTransaction(TransactionDto request) {
        var cardFrom = cardRepository.findByUid(request.fromCardId())
                .orElseThrow(() -> new RuntimeException("Card (from) [" + request.fromCardId() + "] not found"));
        var accountFrom = accountRepository.findByUid(cardFrom.getAccount().getUid())
                .orElseThrow(() -> new RuntimeException("Account (from) [" + request.fromCardId() + "] not found"));
        var cardTo = cardRepository.findByUid(request.toCardId())
                .orElseThrow(() -> new RuntimeException("Card (to) [" + request.toCardId() + "] not found"));
        var accountTo = accountRepository.findByUid(cardTo.getAccount().getUid())
                .orElseThrow(() -> new RuntimeException("Account (to) [" + request.toCardId() + "] not found"));
        var balanceFrom = accountFrom.getBalance();
        if(request.amount() > balanceFrom) {    // вернем на клиента причину ошибки
            throw new RuntimeException("Not enough money on Account (from), balance = [" + balanceFrom + "]");
            //return this.errorResponse(request, 444, "Not enough money on Account (from), balance = [" + balanceFrom + "]");
        }
        accountFrom.setBalance(accountFrom.getBalance() - request.amount());
        accountTo.setBalance(accountTo.getBalance() + request.amount());

        return mapTransaction(transactionRepository.save(Transaction.builder()
                .uid(UUID.randomUUID().toString())
                .fromCardId(cardFrom.getUid())
                .toCardId(cardTo.getUid())
                .amount(request.amount())
                .build()));
    }
/*
    @Transactional
    public TransactionDto updateTransaction(TransactionDto request, String uid) {
        var transaction = transactionRepository.findByUid(uid)
                .orElseThrow(() -> new RuntimeException("Transaction [" + uid + "] not found"));
        transaction.setFromCardId((request.fromCardId() == null) ? transaction.getToCardId() : request.fromCardId());
        transaction.setToCardId((request.toCardId() == null) ? transaction.getFromCardId() : request.toCardId());
        transaction.setAmount((request.amount() <= 0) ? transaction.getAmount() : request.amount());
        return mapTransaction(transactionRepository.save(transaction));
    }

    @Transactional
    public void deleteTransaction(String id) {
        transactionRepository.delete(transactionRepository.findByUid(id).orElse(null));
    }

    private TransactionDto errorResponse(TransactionDto dto, Integer errCode, String errMessage) {
        return new TransactionDto(dto.id(), dto.fromCardId(), dto.toCardId(), dto.amount(), errCode, errMessage);
    }
*/
}
