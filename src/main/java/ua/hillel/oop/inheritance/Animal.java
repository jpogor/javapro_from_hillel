package ua.hillel.oop.inheritance;

public class Animal {
    protected String animalName;
    protected int runRestriction;
    protected int swimRestriction;
    static int count;

    public static int getCount() {
        return count;
    }

    public Animal(String animalName, int runRestriction, int swimRestriction) {
        this.animalName = animalName;
        this.runRestriction = runRestriction;
        this.swimRestriction = swimRestriction;
        count++;
    }
    public void run(int length) {
        if (this.runRestriction <= 0) {
            System.out.println(this.animalName + " не вміє бігати.");
            return;
        }
        length = length <= this.runRestriction ? length : this.runRestriction;
        System.out.println(this.animalName + " пробіг " + length + " м.");
    }

    public void swim(int length) {
        if (this.swimRestriction <= 0) {
            System.out.println(this.animalName + " не вміє плавати.");
            return;
        }
        length = length <= this.swimRestriction ? length : this.swimRestriction;
            System.out.println(this.animalName + " проплив " + length + " м.");
    }
}
