package ua.hillel.oop.inheritance;

public class Cat extends Animal {
    static int countCat;
/*
    @Override
    public static int getCount() {
        return countCat;
    }
*/
    public static int getCountCat() {
        return countCat;
    }
    public Cat(String animalName) {
        super(animalName, 200, 0);
        countCat++;
    }
}
