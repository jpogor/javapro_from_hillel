package ua.hillel.oop.inheritance;

public class Dog extends Animal {
    static int countDog;
    public static int getCountDog() {
        return countDog;
    }
    public Dog(String animalName) {
        super(animalName, 500, 10);
        countDog++;
    }
}
