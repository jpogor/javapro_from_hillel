package ua.hillel.oop.inheritance;

public class Main {
    public static void main(String[] args) {

        Animal animal = new Animal("Цуцик",0,0);
        animal.run(200);
        animal.swim(20);

        System.out.println();

        Cat cat = new Cat("Мурзік");
        cat.run(222);
        cat.swim(10);

        System.out.println();

        Dog dog = new Dog("Тарзан");
        dog.run(555);
        dog.swim(100);

        Dog dog2 = new Dog("Мухтар");
        dog2.run(111);
        dog2.swim(7);

        System.out.println();
        System.out.println("Кількість тварин: " + animal.getCount());
        System.out.println("Кількість котів: " + cat.getCountCat());
        System.out.println("Кількість собак: " + dog.getCountDog());
    }
}
