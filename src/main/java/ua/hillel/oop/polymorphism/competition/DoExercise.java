/*
package ua.hillel.oop.polymorphism.competition;

import ua.hillel.oop.polymorphism.competition.obstacles.Obstacle;
import ua.hillel.oop.polymorphism.competition.obstacles.Track;
import ua.hillel.oop.polymorphism.competition.obstacles.Wall;
import ua.hillel.oop.polymorphism.competition.participants.Participant;

public class DoExercise implements Participant, Obstacle {
    public DoExercise(Participant participant, Obstacle obstacle) {
        if (obstacle instanceof Track)
            participant.run(obstacle.getMax());
        else if (obstacle instanceof Wall)
            participant.jump(obstacle.getMax());
    }

    @Override
    public double getMax() {
        return 0;
    }

    @Override
    public void overcome() {

    }

    @Override
    public double getMaxRun() {
        return 0;
    }

    @Override
    public double getMaxJump() {
        return 0;
    }

    @Override
    public void run(double max) {

    }

    @Override
    public void jump(double max) {

    }
}
*/
