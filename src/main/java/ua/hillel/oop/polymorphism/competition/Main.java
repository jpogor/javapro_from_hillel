package ua.hillel.oop.polymorphism.competition;

import ua.hillel.oop.polymorphism.competition.obstacles.Obstacle;
import ua.hillel.oop.polymorphism.competition.obstacles.Track;
import ua.hillel.oop.polymorphism.competition.obstacles.Wall;
import ua.hillel.oop.polymorphism.competition.participants.Cat;
import ua.hillel.oop.polymorphism.competition.participants.Human;
import ua.hillel.oop.polymorphism.competition.participants.Participant;
import ua.hillel.oop.polymorphism.competition.participants.Robot;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // Массив участников
        ArrayList<Participant> participants = new ArrayList<Participant>();
        participants.add(new Cat("Мурзік",18,0.5));
        participants.add(new Cat("Васька"));
        participants.add(new Human("Петро"));
        participants.add(new Human("Василь", 7000, 2.5));
        participants.add(new Robot("Вертер"));
        participants.add(new Robot("Бартер", 8000, 10));

        // Массив препятствий
        ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
        obstacles.add(new Track(20));
        obstacles.add(new Track(6000));
        obstacles.add(new Track(9000));
        obstacles.add(new Wall(0.2));
        obstacles.add(new Wall(1.5));
        obstacles.add(new Wall(9.0));

        doExercise(participants, obstacles);
    }

    private static void doExercise(ArrayList<Participant> participants, ArrayList<Obstacle> obstacles) {
        for (Obstacle obstacle : obstacles) {
            for (Participant participant : participants) {
                if (obstacle instanceof Track) {
                    participant.run(obstacle.getMax());
                    obstacle.overcome();
                }
                else if (obstacle instanceof Wall) {
                    participant.jump(obstacle.getMax());
                    obstacle.overcome();
                }
            }
        }
    }
}
