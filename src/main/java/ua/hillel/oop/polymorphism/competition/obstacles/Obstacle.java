package ua.hillel.oop.polymorphism.competition.obstacles;

public interface Obstacle {
    public double getMax(); // получить макс высоту/длину препятствия
    void overcome();
}
