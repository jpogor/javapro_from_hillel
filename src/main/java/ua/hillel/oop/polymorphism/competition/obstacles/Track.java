package ua.hillel.oop.polymorphism.competition.obstacles;

public class Track implements Obstacle {
    double max;

    public double getMax() {
        return max;
    }

    public Track(double max) {
        this.max = max;
    }

    @Override
    public void overcome() {
        System.out.println("бігову доріжку " + max + "м.");
    }
}
