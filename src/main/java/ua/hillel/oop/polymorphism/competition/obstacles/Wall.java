package ua.hillel.oop.polymorphism.competition.obstacles;

public class Wall implements Obstacle {
    double max;

    public double getMax() {
        return max;
    }

    public Wall(double max) {
        this.max = max;
    }

    @Override
    public void overcome() {
        System.out.println("стіну висотой " + max + "м.");
    }
}
