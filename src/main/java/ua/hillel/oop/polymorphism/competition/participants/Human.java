package ua.hillel.oop.polymorphism.competition.participants;

public class Human implements Participant {
    static String type = "Людина";
    String name;
    double maxRun;
    double maxJump;

    public double getMaxRun() {
        return maxRun;
    }

    public double getMaxJump() {
        return maxJump;
    }

    public Human(String name, double maxRun, double maxJump) {
        this.name = name;
        this.maxRun = maxRun;
        this.maxJump = maxJump;
    }

    public Human(String name) {
        this(name, 5000, 2.5);
    }

    @Override
    public void run(double max) {
        if (this.maxRun >= max)
            System.out.print(type + " " + name + " пробіг ");
        else
            System.out.print(type + " " + name + " не пробіг ");
    }

    @Override
    public void jump(double max) {
        if (this.maxJump >= max)
            System.out.print(type + " " + name + " перестрибнув ");
        else
            System.out.print(type + " " + name + " не перестрибнув ");
    }
}
