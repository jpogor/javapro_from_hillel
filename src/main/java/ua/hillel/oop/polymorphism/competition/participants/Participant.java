package ua.hillel.oop.polymorphism.competition.participants;

public interface Participant {
    //static String type = "";
    double getMaxRun();
    double getMaxJump();
    void run(double max);
    void jump(double max);
}
