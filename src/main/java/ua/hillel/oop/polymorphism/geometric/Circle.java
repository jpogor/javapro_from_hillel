package ua.hillel.oop.polymorphism.geometric;

public class Circle implements Figure {
    double radius;  // радиус круга

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        double area = Math.PI * (this.radius * this.radius);
        return area;
    }
}
