package ua.hillel.oop.polymorphism.geometric;

public interface Figure {
    public double calculateArea();
}
