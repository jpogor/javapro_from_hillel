package ua.hillel.oop.polymorphism.geometric;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // Створити масив фігур, і написати метод який виведе сумарну площу всіх фігур у цьому масиві
        ArrayList<Figure> figures = new ArrayList<Figure>();

        figures.add(new Square(6));
        figures.add(new Triangle(8, 5));
        figures.add(new Circle(3));
        figures.add(new Square(7));
        figures.add(new Triangle(9, 6));
        figures.add(new Circle(4));

        double area = totalArea(figures);
        System.out.println("Сумарна площа всіх фігур S = " + area);
    }

    private static double totalArea(ArrayList<Figure> figures) {
        double area = 0.0;
        for (Figure figure : figures) {
//        if (figure instanceof Figure)
            area += figure.calculateArea();
        }
        return area;
    }
}