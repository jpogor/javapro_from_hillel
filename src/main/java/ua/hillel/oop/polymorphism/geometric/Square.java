package ua.hillel.oop.polymorphism.geometric;

public class Square implements Figure {
    double side; // сторона квадрата

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        double area = side * side;
        return area;
    }
}
