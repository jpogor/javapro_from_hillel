package ua.hillel.oop.polymorphism.geometric;

public class Triangle implements Figure {
    double base; // основание
    double height; // высота

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public double calculateArea() {
        double area = (base * height) / 2;
        return area;
    }
}
