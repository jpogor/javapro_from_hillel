create table if not exists transactions
(
    id              integer generated by default as identity primary key,
    uid             varchar(36) not null,
    from_card_id    varchar(36) not null,
    to_card_id      varchar(36) not null,
    amount          integer default 0 not null,
    created_at      timestamp default now(),
    updated_at      timestamp default now()
    );

alter table transactions
    owner to hillel;
