select * from Homework
;
select l.*, h.*
from Lesson l
    left join homework h on l.homework_id = h.id
;
select l.*, h.*
from Lesson l
    left join homework h on l.homework_id = h.id
order by l.updatedat
;
select s.*, l.*
from schedule s
    left join schedule_lesson sl on s.id = sl.schedule_id
    left join lesson l on l.id = sl.lesson_id
;
select s.name, count(sl.*)
from schedule s
    left join schedule_lesson sl on s.id = sl.schedule_id
    left join lesson l on sl.lesson_id = l.id
group by s.name
order by 2 desc
;
