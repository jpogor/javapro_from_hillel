--drop table Heroes;
create table if not exists Heroes as table hero
;
select avg(h.height) as avg_height
from Heroes h
;
select max(h.name) as name, max(h.height) as height
from Heroes h
;
select max(h.name) as name, max(h.weight) as weight
from Heroes h
;
select h.gender, count(h.gender) as count
from Heroes h
group by h.gender
;
select h.alignment, count(h.alignment) as count
from Heroes h
group by h.alignment
;
select h.publisher, count(h.alignment) as count
from Heroes h
group by h.publisher
order by 2 desc limit 5
;
select h.haircolor, count(h.haircolor) as count
from Heroes h
group by h.haircolor
order by 2 desc limit 2
;
select h.eyecolor
from Heroes h
group by h.eyecolor
order by count(h.eyecolor) desc limit 1
;
