package ua.hillel.oop.hw10queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoffeeOrderBoardTest {

    private final CoffeeOrderBoard coffeeOrderBoard = new CoffeeOrderBoard();
    String[] names = {"Alen", "Yoda", "Obi-van", "John Snow"};

    @BeforeEach
    void setUp() {
        for(String name : names)
            coffeeOrderBoard.add(name);
    }

    @Test
    void add() {
        boolean result = coffeeOrderBoard.add("R2B2");
        assertEquals(true, result);
        assertEquals(5, coffeeOrderBoard.getOrderCount());
    }

    @Test
    void deliver() {
        Order result = coffeeOrderBoard.deliver();
        assertEquals(new Order(1, names[0]), result);
        assertEquals(4, coffeeOrderBoard.getOrderCount());
    }

    @Test
    void Deliver2() {
        Order result = coffeeOrderBoard.deliver(2);
        assertEquals(new Order(2, names[1]), result);
        assertEquals(4, coffeeOrderBoard.getOrderCount());
    }

    @Test
    void draw() {
        String result = coffeeOrderBoard.draw();
        String expect = "\n====================\nNum | Name\n 1  | Alen\n 2  | Yoda\n 3  | Obi-van\n 4  | John Snow\n";
        assertEquals(expect, result);
    }
}