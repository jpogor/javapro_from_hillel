package ua.hillel.oop.hw14generic.box;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw14generic.fruit.Apple;
import ua.hillel.oop.hw14generic.fruit.Fruit;
import ua.hillel.oop.hw14generic.fruit.Orange;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BoxTest {

    Box<Apple> boxApple = new Box();
    Box<Orange> boxOrange = new Box();
    boolean result;

    @Test
    void toList() {
        Box box = new Box();

        Integer[] intArray = {1, 2, 3, 4, 5};
        List<Integer> integerList = box.toList(intArray);
        assertEquals(List.of(1, 2, 3, 4, 5), integerList);

        String[] strArray = {"one", "two", "three", "for", "five"};
        List<String> stringList = box.toList(strArray);
        assertEquals(List.of("one", "two", "three", "for", "five"), stringList);
    }

    @Test
    void add() {
        Apple apple1 = new Apple();
        result = boxApple.add(apple1);
        assertEquals(true, result);
        assertEquals(1, boxApple.size());
/*      так теперь не получится
        Orange orange1 = new Orange();
        result = boxApple.add(orange1);
        assertEquals(false, result);
        assertEquals(1, boxApple.size());
*/
    }

    @Test
    void addList() {
        List<Apple> fruits1 = List.of(new Apple(), new Apple(), new Apple());
        result = boxApple.add(fruits1);
        assertEquals(true, result);
        assertEquals(3, boxApple.size());

        List<Orange> fruits2 = List.of(new Orange(), new Orange());
        result = boxOrange.add(fruits2);
        assertEquals(true, result);
        assertEquals(2, boxOrange.size());
    }

    @Test
    void getWeight  () {
        addList();
        assertEquals(3, boxApple.getWeight());
        assertEquals(3, boxOrange.getWeight());
    }

    @Test
    void compare() {
        addList();
        result = boxApple.compare(boxOrange);
        assertEquals(true, result);
    }

    @Test
    void merge() {
        addList();
        Box<Apple> boxAppleNew = new Box();
        boxAppleNew = boxAppleNew.merge(boxApple);
        assertEquals(3F, boxAppleNew.getWeight());
        assertEquals(0F, boxApple.getWeight());
    }

    @Test
    void size() {
        addList();
        assertEquals(3, boxApple.size());
        assertEquals(2, boxOrange.size());
    }
}