package ua.hillel.oop.hw16streams;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductsTest {

    private final Products products = new Products();

    private void init() {
        products.add(new Product("Book", 255.0, 5, LocalDate.parse("2022-04-03")));
        products.add(new Product("Book", 300.0, 10, LocalDate.parse("2020-10-15")));
        products.add(new Product("Journal", 30.0, 0, LocalDate.parse("2021-08-25")));
        products.add(new Product("Toy", 240.0, 0, LocalDate.parse("2017-03-14")));
        products.add(new Product("Book", 260.0, 15, LocalDate.parse("2015-09-27")));
        products.add(new Product("Book", 75.0, 10,  LocalDate.parse("2022-07-09")));
        products.add(new Product("Toy", 80.0, 0, LocalDate.parse("2020-02-13")));
        products.add(new Product("Book", 320.0, 25, LocalDate.parse("2019-12-16")));
        products.add(new Product("Journal", 55.0, 10,  LocalDate.parse("2021-04-04")));
        products.add(new Product("Book", 175.0, 0, LocalDate.parse("2022-11-17")));
        products.add(new Product("Toy", 55.0, 0, LocalDate.parse("2020-08-01")));
    }

    @Test
    void getExpensive() {
        init();
        var result = products.getExpensive("Book", 250);
        var expect = List.of(
                products.getProducts().get(0),
                products.getProducts().get(1),
                products.getProducts().get(4),
                products.getProducts().get(7)
                );
        assertEquals(expect, result);
    }

    @Test
    void getDiscount() {
        init();
        var result = products.getDiscount("Book", 10);
        var expect = List.of(
                new Product("Book", 270.0, 10, LocalDate.parse("2020-10-15")),
                new Product("Book", 221.0, 15, LocalDate.parse("2015-09-27")),
                new Product("Book", 67.5, 10, LocalDate.parse("2022-07-09")),
                new Product("Book", 240.0, 25, LocalDate.parse("2019-12-16"))
                );
        assertEquals(expect, result);
    }

    @Test
    void getCheapest_v1() {
        init();
        var result = products.getCheapest_v1("Book");
        var expect = List.of(
                products.getProducts().get(5)
                );
        assertEquals(expect, result);
    }

    @Test
    void getCheapest_v2() {
        init();
        var result = products.getCheapest_v2("Book");
        var expect = products.getProducts().get(5);
        assertEquals(expect, result);
   }

    @Test
    void getLastCreated() {
        init();
        var result = products.getLastCreated(3);
        var expect = List.of(
                products.getProducts().get(9),
                products.getProducts().get(5),
                products.getProducts().get(0)
        );
        assertEquals(expect, result);
    }

    @Test
    void doCalc() {
        init();
        var result = products.doCalc("Book", 2022, 175);
        var expect = 250.0;
        assertEquals(expect, result);
    }

    @Test
    void doGroup() {
        init();
        Map<String, List<Product>> result = products.doGroup();
        Map<String, List<Product>> expect = new HashMap<>();
        expect.put("Book", List.of(
                products.getProducts().get(0),
                products.getProducts().get(1),
                products.getProducts().get(4),
                products.getProducts().get(5),
                products.getProducts().get(7),
                products.getProducts().get(9)
        ));
        expect.put("Journal", List.of(
                products.getProducts().get(2),
                products.getProducts().get(8)
        ));
        expect.put("Toy", List.of(
                products.getProducts().get(3),
                products.getProducts().get(6),
                products.getProducts().get(10)
        ));
        assertEquals(expect, result);
    }
}