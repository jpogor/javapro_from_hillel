package ua.hillel.oop.hw17multithreading;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw15multithreading.ArrayInitializer;

import static org.junit.jupiter.api.Assertions.*;

class ArrayInitializer2Test {

    @Test
    void init() {
        double[] array = new double[101];
        for (int i = 0; i < array.length; i++)
            array[i] = Math.PI;
        ArrayInitializer.init(array);
        for (int i = 0; i < array.length / 2; i++) {
            assertEquals(array[i], array[array.length / 2 + i]);
        }
    }
}