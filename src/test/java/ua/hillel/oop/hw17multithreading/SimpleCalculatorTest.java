package ua.hillel.oop.hw17multithreading;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw15multithreading.ArrayInitializer;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleCalculatorTest {
    @Test
    void init() {
        int result = SimpleCalculator.squareSum(12, 18);
        assertEquals(468, result);
    }
}