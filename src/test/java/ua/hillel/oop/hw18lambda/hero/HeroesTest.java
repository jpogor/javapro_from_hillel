package ua.hillel.oop.hw18lambda.hero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HeroesTest {

    private final Heroes heroes = new Heroes();

    @BeforeEach
    public void init() {
        heroes.add(new Hero("Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110));
        heroes.add(new Hero("Hero_02", "Male", "Red", "Black", "Black", 100.10, "Elephant", "Bronse", "Good", 55));
        heroes.add(new Hero("Hero_03", "Female", "Brown", "Brown", "Brown", 180.00, "Marvell", "Bronse", "Good", 210));
        heroes.add(new Hero("Hero_04", "Male", "Blue", "Black", "Black", 170.00, "Marvell", "Bronse", "Evil", 100));
        heroes.add(new Hero("Hero_05", "Male", "Blue", "Red", "Black", 210.00, "Marvell", "Bronse", "Good", 270));
        heroes.add(new Hero("Hero_06", "Female", "Rad", "Black", "Black", 188.00, "Marvell", "Bronse", "Good", 95));
        heroes.add(new Hero("Hero_07", "Male", "Blue", "Black", "Black", 95.00, "Cartoon", "Bronse", "Evil", 120));
        heroes.add(new Hero("Hero_08", "Male", "Brown", "Brown", "Brown", 195.00, "Marvell", "Bronse", "Good", 190));
        heroes.add(new Hero("Hero_09", "Female", "Blue", "Red", "Black", 155.00, "Elephant", "Bronse", "Good", 140));
        heroes.add(new Hero("Hero_10", "Male", "Blue", "Black", "Black", 177.00, "Marvell", "Bronse", "Evil", 185));
        heroes.add(new Hero("Hero_11", "Female", "Blue", "Black", "Black", 166.00, "Marvell", "Bronse", "Good", 106));
        heroes.add(new Hero("Hero_12", "Male", "Rad", "Red", "Black", 112.00, "Cartoon", "Bronse", "Good", 150));
        heroes.add(new Hero("Hero_13", "Male", "Blue", "Black", "Black", 115.00, "Marvell", "Bronse", "Good", 125));
        heroes.add(new Hero("Hero_14", "Male", "Blue", "Black", "Black", 220.00, "Cartoon", "Bronse", "Evil", 177));
        heroes.add(new Hero("Hero_15", "Female", "Brown", "Brown", "Brown", 200.00, "Marvell", "Bronse", "Good", 255));
    }

    @Test
    void getAverageAgeOfHeroes() {
        var result = heroes.getAverageAgeOfHeroes();
        var expected = 165.67466666666667;
        assertEquals(expected, result);
    }

    @Test
    void getNameOfTheGreatestHero() {
        var result = heroes.getNameOfTheTallestHero();
        var expected = "Hero_14";
        assertEquals(expected, result);
    }

    @Test
    void getNameOfTheHardestHero() {
        var result = heroes.getNameOfTheHeavestHero();
        var expected = "Hero_05";
        assertEquals(expected, result);
    }

    @Test
    void getNumberOfHeroesInEachGenderGroup() {
        var result = new HashMap<String, Long>();
        result.put("Female", 5L);
        result.put("Male", 10L);
        var expected = heroes.getNumberOfHeroesInEachGenderGroup();
        assertEquals(expected, result);
    }

    @Test
    void getNumberOfHeroesInEachGroup() {
        var result = new HashMap<String, Long>();
        result.put("Evil", 4L);
        result.put("Good", 11L);
        var expected = heroes.getNumberOfHeroesInEachGroup();
        assertEquals(expected, result);
    }

    @Test
    void getNamesOfMostPopularPublishers() {
        var result = List.of("Marvell","Cartoon");
        var expected = heroes.getNamesOfMostPopularPublishers(2);
        assertEquals(expected, result);
    }

    @Test
    void getMostCommonHairColors() {
        var result = List.of("Black");
        var expected = heroes.getMostCommonHairColors(1);
        assertEquals(expected, result);
    }

    @Test
    void getMostCommonEyeColor() {
        var result = List.of("Blue", "Brown");
        var expected = heroes.getMostCommonEyeColor(2);
        assertEquals(expected, result);
    }
}