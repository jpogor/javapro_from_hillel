package ua.hillel.oop.hw19quiksort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuikSortTest {

    @Test
    void execute() {
        int[] result = { 15, 38, 21, 7, 77, 64, 51, 19, 7 };
        var q1 = new QuikSort();
        q1.execute(result, 0, result.length - 1);
        int[] expected = { 7, 7, 15, 19, 21, 38, 51, 64, 77 };
        assertArrayEquals(expected, result);
    }

    @Test
    void execute2() {
        int[] result = { 33, -12, 0, -7, 77, -64, 51, 19, 7 };
        var q1 = new QuikSort();
        q1.execute(result, 0, result.length - 1);
        //q1.printArray(result);
        int[] expected = { -64, -12, -7, 0, 7, 19, 33, 51, 77 };
        assertArrayEquals(expected, result);
    }

}