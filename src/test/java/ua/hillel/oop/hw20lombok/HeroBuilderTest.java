package ua.hillel.oop.hw20lombok;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HeroBuilderTest {

    @Test
    void builder() {
        var result = HeroBuilder.builder()
                .name("Hero_01")
                .gender("Male")
                .eyeColor("Blue")
                .race("Red")
                .hairColor("Black")
                .height(202.02)
                .publisher("Marvell")
                .skinColor("Bronze")
                .alignment("Good")
                .weight(110)
                .build();

        var expected = new HeroBuilder(
                "Hero_01",
                "Male",
                "Blue",
                "Red",
                "Black",
                202.02,
                "Marvell",
                "Bronze",
                "Good",
                110);

        assertEquals(expected, result);
    }
}