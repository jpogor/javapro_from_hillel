package ua.hillel.oop.hw20lombok;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroLombokTest {

    private HeroLombok hero;

    @BeforeEach
    public void init() {
        hero = new HeroLombok("Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110);
    }

    @Test
    void testEquals() {
        var heroLombok2 = new HeroLombok("Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110);
        assertEquals(heroLombok2, hero);
    }

    @Test
    void getName() {
        assertEquals("Hero_01", hero.getName());
    }

    @Test
    void getGender() {
        assertEquals("Male", hero.getGender());
    }

    @Test
    void getEyeColor() {
        assertEquals("Blue", hero.getEyeColor());
    }

    @Test
    void getRace() {
        assertEquals("Red", hero.getRace());
    }

    @Test
    void getHairColor() {
        assertEquals("Black", hero.getHairColor());
    }

    @Test
    void getHeight() {
        assertEquals(202.02, hero.getHeight());
    }

    @Test
    void getPublisher() {
        assertEquals("Marvell", hero.getPublisher());
    }

    @Test
    void getSkinColor() {
        assertEquals("Bronse", hero.getSkinColor());
    }

    @Test
    void getAlignment() {
        assertEquals("Good", hero.getAlignment());
    }

    @Test
    void getWeight() {
        assertEquals(110, hero.getWeight());
    }

    @Test
    void setName() {
        hero.setName("Hero_Test");
        assertEquals("Hero_Test", hero.getName());
    }

    @Test
    void setGender() {
        hero.setGender("Female");
        assertEquals("Female", hero.getGender());
    }

    @Test
    void setEyeColor() {
        hero.setEyeColor("Red");
        assertEquals("Red", hero.getEyeColor());
    }

    @Test
    void setRace() {
        hero.setRace("Terran");
        assertEquals("Terran", hero.getRace());
    }

    @Test
    void setHairColor() {
        hero.setHairColor("Red");
        assertEquals("Red", hero.getHairColor());
    }

    @Test
    void setHeight() {
        hero.setHeight(200);
        assertEquals(200, hero.getHeight());
    }

    @Test
    void setPublisher() {
        hero.setPublisher("CNN");
        assertEquals("CNN", hero.getPublisher());
    }

    @Test
    void setSkinColor() {
        hero.setSkinColor("Red");
        assertEquals("Red", hero.getSkinColor());
    }

    @Test
    void setAlignment() {
        hero.setAlignment("Bad");
        assertEquals("Bad", hero.getAlignment());
    }

    @Test
    void setWeight() {
        hero.setWeight(200);
        assertEquals(200, hero.getWeight());
    }

    @Test
    void testToString() {
        hero.toString();
        assertEquals("HeroLombok(name=Hero_01, gender=Male, eyeColor=Blue, race=Red, hairColor=Black, height=202.02, publisher=Marvell, skinColor=Bronse, alignment=Good, weight=110)", hero.toString());
    }
}