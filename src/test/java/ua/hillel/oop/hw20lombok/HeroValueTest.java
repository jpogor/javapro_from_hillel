package ua.hillel.oop.hw20lombok;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroValueTest {

    private HeroValue hero;

    @BeforeEach
    public void init() {
        hero = new HeroValue("Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110);
    }

    @Test
    void getName() {
        assertEquals("Hero_01", hero.getName());
    }

    @Test
    void getGender() {
        assertEquals("Male", hero.getGender());
    }

    @Test
    void getEyeColor() {
        assertEquals("Blue", hero.getEyeColor());
    }

    @Test
    void getRace() {
        assertEquals("Red", hero.getRace());
    }

    @Test
    void getHairColor() {
        assertEquals("Black", hero.getHairColor());
    }

    @Test
    void getHeight() {
        assertEquals(202.02, hero.getHeight());
    }

    @Test
    void getPublisher() {
        assertEquals("Marvell", hero.getPublisher());
    }

    @Test
    void getSkinColor() {
        assertEquals("Bronse", hero.getSkinColor());
    }

    @Test
    void getAlignment() {
        assertEquals("Good", hero.getAlignment());
    }

    @Test
    void getWeight() {
        assertEquals(110, hero.getWeight());
    }

    @Test
    void testToString() {
        hero.toString();
        assertEquals("HeroValue(name=Hero_01, gender=Male, eyeColor=Blue, race=Red, hairColor=Black, height=202.02, publisher=Marvell, skinColor=Bronse, alignment=Good, weight=110)", hero.toString());
    }
}