package ua.hillel.oop.hw25patterns;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw24jdbc.Hero;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HeroServiceIntegrationTest {

    @Test
    void shouldReturnListOfHeroes() {

        var heroMovies = new HashMap<String, List<String>>();
        heroMovies.put("Hero_01", List.of("Film_01", "Film_02", "Film_03"));
        heroMovies.put("Hero_02", List.of("Film_03"));
        heroMovies.put("Hero_03", List.of("Film_01", "Film_03"));
        HeroMovieService heroMovieService = new HeroMovieService(heroMovies);

        var target = new HeroFabric(heroMovieService).createService(List.of(
                new Hero(1L, "Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronze", "Good", 110),
                new Hero(2L, "Hero_02", "Male", "Red", "Black", "Black", 100.10, "Elephant", "Bronze", "Good", 55),
                new Hero(3L, "Hero_03", "Female", "Brown", "Brown", "Brown", 180.00, "Marvell", "Bronze", "Good", 210)
        ));

        List<HeroDto> result = target.getHeroes();
        List<HeroDto> expected = List.of(
                new HeroDto(1L, "Hero_01", "Male", List.of("Film_01", "Film_02", "Film_03")),
                new HeroDto(2L, "Hero_02", "Male", List.of("Film_03")),
                new HeroDto(3L, "Hero_03", "Female", List.of("Film_01", "Film_03"))
        );
        assertEquals(expected, result);
    }
}