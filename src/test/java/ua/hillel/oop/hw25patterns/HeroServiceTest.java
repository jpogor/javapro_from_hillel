package ua.hillel.oop.hw25patterns;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.hillel.oop.hw24jdbc.Hero;
import ua.hillel.oop.hw24jdbc.HeroDao;
import ua.hillel.oop.hw24jdbc.HeroDaoImpl;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static ua.hillel.oop.hw25patterns.HeroFabric.createDataSource;

@ExtendWith(MockitoExtension.class)
class HeroServiceTest {

    @Mock
    private HeroDao heroDao;

    @Mock
    private HeroMovieService heroMovieService;

    @Test
    void getHeroes() {

        List<Hero> heroes = List.of(
                new Hero(1L, "Hero_01", "Male", "Blue", "Red", "Black", 202.02, "Marvell", "Bronse", "Good", 110),
                new Hero(2L, "Hero_02", "Male", "Red", "Black", "Black", 100.10, "Elephant", "Bronse", "Good", 55),
                new Hero(3L, "Hero_03", "Female", "Brown", "Brown", "Brown", 180.00, "Marvell", "Bronse", "Good", 210),
                new Hero(4L, "Hero_04", "Male", "Blue", "Black", "Black", 170.00, "Marvell", "Bronse", "Evil", 100),
                new Hero(5L, "Hero_05", "Male", "Blue", "Red", "Black", 210.00, "Marvell", "Bronse", "Good", 270)
        );
        when(heroDao.findAll()).thenReturn(heroes);
        when(heroMovieService.getPlayedIn("Hero_01")).thenReturn(List.of("Film_01", "Film_02", "Film_03"));
        when(heroMovieService.getPlayedIn("Hero_02")).thenReturn(List.of("Film_01", "Film_03", "Film_04"));
        when(heroMovieService.getPlayedIn("Hero_03")).thenReturn(List.of("Film_03", "Film_04", "Film_05"));
        when(heroMovieService.getPlayedIn("Hero_04")).thenReturn(List.of("Film_02", "Film_03", "Film_04"));
        when(heroMovieService.getPlayedIn("Hero_05")).thenReturn(List.of("Film_01", "Film_03", "Film_05"));

        HeroService heroService = new HeroService(heroDao, heroMovieService);
        List<HeroDto> result = heroService.getHeroes();
        var expected = List.of(
                new HeroDto(1L,"Hero_01", "Male", List.of("Film_01", "Film_02", "Film_03")),
                new HeroDto(2L,"Hero_02", "Male", List.of("Film_01", "Film_03", "Film_04")),
                new HeroDto(3L,"Hero_03", "Female", List.of("Film_03", "Film_04", "Film_05")),
                new HeroDto(4L,"Hero_04", "Male", List.of("Film_02", "Film_03", "Film_04")),
                new HeroDto(5L,"Hero_05", "Male", List.of("Film_01", "Film_03", "Film_05"))
                );
        assertEquals(expected, result);
    }
}
