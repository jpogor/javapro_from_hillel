package ua.hillel.oop.hw33bank.account;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw33bank.WebIntegrationTest;
import ua.hillel.oop.hw33bank.person.Person;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AccountControllerIntegrationTest extends WebIntegrationTest {
    @Test
    void findAll() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());
        var account = accountRepository.save(Account.builder()
                .uid(UUID.randomUUID().toString())
                .iban("UA15625896900000008877655453536666")
                .balance(111L)
                .person(person)
                .build());

        var request = get("/api/accounts/" + account.getUid());
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.iban", equalTo(account.getIban())))
                .andExpect(jsonPath("$.balance", equalTo(account.getBalance().intValue())))
                .andExpect(jsonPath("$.person.name", equalTo(account.getPerson().getName())));
    }

    @Test
    void createAccount() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Mike Peterson")
                .email("mp@test.com")
                .build());
        var account = new AccountDto(null, "UA15625896900000008877655453538888", 1L, person.getUid(), null);

        var request = post("/api/accounts")
                .content(objectMapper.writeValueAsBytes(account))
                .contentType(APPLICATION_JSON);
        var body = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.iban", equalTo(account.iban())))
                .andExpect(jsonPath("$.balance", equalTo(account.balance().intValue())))
                .andExpect(jsonPath("$.personId", equalTo(person.getUid())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        var entityId = objectMapper.readValue(body, AccountDto.class).id();
        var persistedAccount = accountRepository.findByUid(entityId).orElseThrow();
        assertThat(persistedAccount.getIban(), equalTo(account.iban()));
        assertThat(persistedAccount.getBalance(), equalTo(account.balance()));
        assertThat(persistedAccount.getCreatedAt(), is(notNullValue()));
        assertThat(persistedAccount.getUpdatedAt(), is(notNullValue()));
    }

    @Test
    void updateAccount() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());
        var account = accountRepository.save(Account.builder()
                .uid(UUID.randomUUID().toString())
                .iban("UA15625896900000008877655453536666")
                .balance(111L)
                .person(person)
                .build());
        AccountDto accountDTO = new AccountDto(account.getUid(), "UA15625896900000008877655453536666", 111L, account.getPerson().getUid(), null);

        var request = put("/api/accounts/" + account.getUid())
                .content(objectMapper.writeValueAsBytes(accountDTO))
                .contentType(APPLICATION_JSON);
        var body = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.iban", equalTo(accountDTO.iban())))
                .andExpect(jsonPath("$.balance", equalTo(accountDTO.balance().intValue())))
                .andExpect(jsonPath("$.personId", equalTo(accountDTO.personId())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        var entityId = objectMapper.readValue(body, AccountDto.class).id();
        var persistedAccount = accountRepository.findByUid(entityId).orElseThrow();
        assertThat(persistedAccount.getIban(), equalTo(account.getIban()));
        assertThat(persistedAccount.getBalance(), equalTo(account.getBalance()));
        assertThat(persistedAccount.getCreatedAt(), is(notNullValue()));
        assertThat(persistedAccount.getUpdatedAt(), is(notNullValue()));
    }

    @Test
    void deleteAccount() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());
        var account = accountRepository.save(Account.builder()
                .uid(UUID.randomUUID().toString())
                .iban("UA15625896900000008877655453536666")
                .balance(111L)
                .person(person)
                .build());

        var request = delete("/api/accounts/" + account.getUid());
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }
}