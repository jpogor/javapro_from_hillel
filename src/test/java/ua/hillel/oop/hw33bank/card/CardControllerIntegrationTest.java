package ua.hillel.oop.hw33bank.card;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw33bank.WebIntegrationTest;
import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.person.Person;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.text.CharSequenceLength.hasLength;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
class CardControllerIntegrationTest extends WebIntegrationTest {

    @Test
    void createCard() throws Exception {
        var person = personRepository.save(Person.builder()
                .name("TestUser")
                .uid("personUID")
                .email("test@email.com")
                .accounts(List.of())
                .build());
        var account = accountRepository.save(Account.builder()
                .uid("accountUID")
                .iban("UA123456789")
                .balance(1L)
                .person(person)
                .build());
        var request = new CardDto(null, null, account.getUid(), person.getUid(), (short) 0, (short) 0, null, null);
        var body = mockMvc.perform(post("/api/cards")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.account_id", equalTo(account.getUid())))
                .andExpect(jsonPath("$.person_id", equalTo(person.getUid())))
                .andExpect(jsonPath("$.pan", is(notNullValue())))
                .andExpect(jsonPath("$.pin", is(notNullValue())))
                .andExpect(jsonPath("$.cvv", is(notNullValue())))
                .andExpect(jsonPath("$.expiration_date", is(notNullValue())))
                .andReturn()
                .getResponse()
                .getContentAsString();
        var entityId = objectMapper.readValue(body, CardDto.class).id();
        var resultCard = cardRepository.findByUid(entityId).orElseThrow();
        assertThat(resultCard.getUid(), CoreMatchers.notNullValue());
        assertThat(resultCard.getPan(), CoreMatchers.notNullValue());
        assertThat(String.valueOf(resultCard.getPan()), hasLength(16));
        assertThat(resultCard.getPin(), CoreMatchers.notNullValue());
        assertThat(String.valueOf(resultCard.getPin()), hasLength(4));
        assertThat(resultCard.getCvv(), CoreMatchers.notNullValue());
        assertThat(String.valueOf(resultCard.getCvv()), hasLength(3));
        assertThat(resultCard.getExpirationDate(), CoreMatchers.notNullValue());
    }
}