package ua.hillel.oop.hw33bank.card;

import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.account.AccountRepository;
import ua.hillel.oop.hw33bank.person.Person;
import ua.hillel.oop.hw33bank.person.PersonRepository;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.CharSequenceLength.hasLength;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CardServiceTest {
    @Mock
    private CardRepository cardRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private PersonRepository personRepository;

    private CardService cardService;
    @Captor
    ArgumentCaptor<Card> argumentCaptorCard;

    @BeforeEach
    void setUp() {
        cardService = new CardService(cardRepository, accountRepository, personRepository);
    }

    @Test
    void findCard() {
    }

    @Test
    void getCard() {
    }

    @Test
    void openCard() {
        when(personRepository.findByUid("personUID")).thenReturn(Optional.ofNullable(Person.builder()
                .name("TestUser")
                .uid("personUID")
                .email("test@email.com")
                .accounts(List.of())
                .build()));
        when(accountRepository.findByUid("accountUID")).thenReturn(Optional.ofNullable(Account.builder()
                .uid("accountUID")
                .iban("UA123456789")
                .balance(1L)
//                .person(personRepository.findByUid("personUID").get())
                .person(Person.builder()
                        .name("TestUser")
                        .uid("personUID")
                        .email("test@email.com")
                        .accounts(List.of())
                        .build())
                .build()));
        when(cardRepository.save(Mockito.any())).thenAnswer(invocation -> invocation.getArguments()[0]);
        CardDto cardDto = new CardDto(null, null, "accountUID", "personUID", (short) 0, (short) 0, null, null);
        var result = cardService.openCard(cardDto);
        assertThat(result.id(), CoreMatchers.notNullValue());
        assertEquals("accountUID", result.accountId());
        assertThat(result.expirationDate(), CoreMatchers.notNullValue());
        assertThat(result.pin(), CoreMatchers.notNullValue());
        assertThat(String.valueOf(result.pin()), hasLength(4));
        assertThat(result.cvv(), CoreMatchers.notNullValue());
        assertThat(String.valueOf(result.cvv()), hasLength(3));
        Mockito.verify(accountRepository).findByUid("accountUID");
        Mockito.verify(cardRepository).save(argumentCaptorCard.capture());
        var card = argumentCaptorCard.getValue();
        assertThat(card.getPan(), CoreMatchers.notNullValue());
        assertThat(card.getPerson(), CoreMatchers.notNullValue());
    }

    @Test
    void updateCard() {
    }

    @Test
    void deleteCard() {
    }
}