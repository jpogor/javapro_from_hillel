package ua.hillel.oop.hw33bank.currency.converter;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;
import ua.hillel.oop.hw33bank.WebIntegrationTest;

import java.io.*;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CurrencyApiCurrencyConverterIntegrationTest extends WebIntegrationTest {

    String jsonData;
    @BeforeEach
    void init() throws IOException {
        // Получить строку из json-файла
        /*  Вариант 1
                String jsonData = new String(Files.readAllBytes(Paths.get("./src/main/resources/test.json")));
        */
        //  Вариант 2
                Resource resource = new DefaultResourceLoader().getResource("classpath:test.json");
                try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
                    jsonData = FileCopyUtils.copyToString(reader);
                }
        /*  Вариант 3
                InputStream inputStream = CurrencyApiCurrencyConverterIntegrationTest.class.getResourceAsStream("/test.json");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                String jsonData = stringBuilder.toString();
        */
        /*  Вариант 4
                String jsonData;
                InputStream inputStream = CurrencyApiCurrencyConverterIntegrationTest.class.getResourceAsStream("/test.json");
                try (Reader reader = new InputStreamReader(inputStream, UTF_8)) {
                    jsonData = FileCopyUtils.copyToString(reader);
                }
        */
    }

    @Test
    void convert() throws Exception {
        wireMockServer.stubFor(WireMock.get(urlEqualTo("/v3/latest?base_currency=USD&currencies=UAH&apikey="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(jsonData)));
        var request = get("/api/currency/convert")
                .queryParam("from", "USD")
                .queryParam("to", "UAH")
                .queryParam("amount", String.valueOf(100));
        var result = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        var res = Double.parseDouble(result);
        assertEquals(3668.5576729, res);
    }

    @Test
    void convert_v2() throws Exception {
        wireMockServer.stubFor(WireMock.get(urlEqualTo("/v3/latest?base_currency=USD&currencies=UAH&apikey="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(jsonData)));
        var request = get("/api/currency/convert_v2")
                .queryParam("from", "USD")
                .queryParam("to", "UAH")
                .queryParam("amount", String.valueOf(100));
        var result = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        var res = Double.parseDouble(result);
        assertEquals(3668.5576729, res);
    }
}
