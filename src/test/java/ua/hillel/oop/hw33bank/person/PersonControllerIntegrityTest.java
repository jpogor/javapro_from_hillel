package ua.hillel.oop.hw33bank.person;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw33bank.WebIntegrationTest;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
class PersonControllerIntegrityTest extends WebIntegrationTest {

    @Test
    void getAll() {
    }

    @Test
    void findAll() throws Exception {
        cardRepository.deleteAll();     // нужно чистить, т.к. в других тестах создаются новые счета
        accountRepository.deleteAll();  // нужно чистить, т.к. в других тестах создаются новые счета
        personRepository.deleteAll();   // нужно чистить, т.к. в других тестах создаются новые персоны
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());
        var request = get("/api/persons");
        var body = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[:1].id", equalTo(List.of(person.getUid()))))
                .andExpect(jsonPath("$[:1].name", equalTo(List.of(person.getName()))))
                .andExpect(jsonPath("$[:1].email", equalTo(List.of(person.getEmail()))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        log.info("body = " + body);
    }

    @Test
    void person() {
    }

    @Test
    void createPerson() throws Exception {
        var request = new PersonDto(null, "Test", "test_" + UUID.randomUUID().toString().substring(0,3) + "@example.com", null);
        var body = mockMvc.perform(post("/api/persons")
                        .content(objectMapper.writeValueAsString(request))
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.name", equalTo(request.name())))
                .andExpect(jsonPath("$.email", equalTo(request.email())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        var entityId = objectMapper.readValue(body, PersonDto.class).id();
        var persistedPerson = personRepository.findByUid(entityId).orElseThrow();
        assertThat(persistedPerson.getName(), equalTo(request.name()));
        assertThat(persistedPerson.getEmail(), equalTo(request.email()));
        assertThat(persistedPerson.getCreatedAt(), is(notNullValue()));
        assertThat(persistedPerson.getUpdatedAt(), is(notNullValue()));
    }

    @Test
    void updatedPerson() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());
        var request = new PersonDto(person.getUid(), "Vasiliy Pupkin", "vp@test.ua", null);
        var body = mockMvc.perform(put("/api/persons/" + person.getUid())
                        .content(objectMapper.writeValueAsBytes(request))
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(request.id())))
                .andExpect(jsonPath("$.name", equalTo(request.name())))
                .andExpect(jsonPath("$.email", equalTo(request.email())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        var entityId = objectMapper.readValue(body, PersonDto.class).id();
        var persistedPerson = personRepository.findByUid(entityId).orElseThrow();
        assertThat(persistedPerson.getName(), equalTo(request.name()));
        assertThat(persistedPerson.getEmail(), equalTo(request.email()));
        assertThat(persistedPerson.getCreatedAt(), is(notNullValue()));
        assertThat(persistedPerson.getUpdatedAt(), is(notNullValue()));
/*
        var request = put("/api/persons/" + person.getUid())
                .content(objectMapper.writeValueAsBytes(personDto))
                .contentType(APPLICATION_JSON);
        var body = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(personDto.id())))
                .andExpect(jsonPath("$.name", equalTo(personDto.name())))
                .andExpect(jsonPath("$.email", equalTo(personDto.email())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        var entityId = objectMapper.readValue(body, PersonDto.class).id();
        var persistedPerson = personRepository.findByUid(entityId).orElseThrow();
        assertThat(persistedPerson.getName(), equalTo(personDto.name()));
        assertThat(persistedPerson.getEmail(), equalTo(personDto.email()));
        assertThat(persistedPerson.getCreatedAt(), is(notNullValue()));
        assertThat(persistedPerson.getUpdatedAt(), is(notNullValue()));
*/
    }

    @Test
    void deletePerson() throws Exception {
        var person = personRepository.save(Person.builder()
                .uid(UUID.randomUUID().toString())
                .name("Ivan Petrov")
                .email("ip@test.com")
                .build());

        var request = delete("/api/persons/" + person.getUid());
        var body = mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertThat(body, is(""));
    }
}