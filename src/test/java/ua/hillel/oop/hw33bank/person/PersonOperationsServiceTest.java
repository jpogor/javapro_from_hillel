package ua.hillel.oop.hw33bank.person;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw33bank.WebIntegrationTest;
import ua.hillel.oop.hw33bank.currency.converter.DummyCurrencyConverter;

import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

class PersonOperationsServiceTest extends WebIntegrationTest {

    @Test
    void convert() throws Exception {
        var personOperationsService = new PersonOperationsService(new DummyCurrencyConverter());
        var result = personOperationsService.convert(Currency.getInstance("USD"), Currency.getInstance("UAH"), 150);
        assertEquals(6000, result);
    }
}