package ua.hillel.oop.hw33bank.transaction;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw33bank.WebIntegrationTest;
import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.card.Card;
import ua.hillel.oop.hw33bank.card.CardStatus;
import ua.hillel.oop.hw33bank.person.Person;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TransactionControllerTest extends WebIntegrationTest {

    @Test
    void newTransaction() throws Exception {
        var person1 = personRepository.save(Person.builder()
                .uid("personUID1")
                .name("TestUser1")
                .email("test1@email.com")
                .accounts(List.of())
                .build());
        var account1 = accountRepository.save(Account.builder()
                .uid("account_UID1")
                .iban("UA77770000_UID1")
                .balance(1100L)
                .person(person1)
                .build());
        var card1 = cardRepository.save(Card.builder()
                .uid("card_UID1")
                .pan("7777000000000001")
                .account(account1)
                .expirationDate(Instant.now().plus(364, ChronoUnit.DAYS))
                .pin((short) 1111)
                .cvv((short) 111)
                .status(CardStatus.ACTIVE)
                .person(person1)
                .build());

        var person2 = personRepository.save(Person.builder()
                .uid("personUID2")
                .name("TestUser2")
                .email("test2@email.com")
                .accounts(List.of())
                .build());
        var account2 = accountRepository.save(Account.builder()
                .uid("account_UID2")
                .iban("UA77770000_UID2")
                .balance(0L)
                .person(person2)
                .build());
        var card2 = cardRepository.save(Card.builder()
                .uid("card_UID2")
                .pan("7777000000000002")
                .account(account2)
                .expirationDate(Instant.now().plus(364, ChronoUnit.DAYS))
                .pin((short) 1111)
                .cvv((short) 111)
                .status(CardStatus.ACTIVE)
                .person(person2)
                .build());

        var requestBody = new TransactionDto(null, card1.getUid(), card2.getUid(), 100L);
        var request = post("/api/transactions")
                .content(objectMapper.writeValueAsBytes(requestBody))
                .contentType("application/json");
        mockMvc.perform(request)
                .andExpect(status().isOk());

        var balance1 = accountRepository.findByUid(account1.getUid()).orElseThrow().getBalance();
        var balance2 = accountRepository.findByUid(account2.getUid()).orElseThrow().getBalance();
        assertEquals(1000L, balance1);
        assertEquals(100L, balance2);
    }
}
