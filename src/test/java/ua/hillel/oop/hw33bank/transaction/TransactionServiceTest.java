package ua.hillel.oop.hw33bank.transaction;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.hillel.oop.hw33bank.account.Account;
import ua.hillel.oop.hw33bank.account.AccountRepository;
import ua.hillel.oop.hw33bank.card.Card;
import ua.hillel.oop.hw33bank.card.CardRepository;
import ua.hillel.oop.hw33bank.card.CardStatus;
import ua.hillel.oop.hw33bank.person.Person;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.text.CharSequenceLength.hasLength;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
class TransactionServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private CardRepository cardRepository;
    @Mock
    private TransactionRepository transactionRepository;

    private TransactionService transactionService;
    @Captor
    ArgumentCaptor<Transaction> argumentCaptorTransaction;

    @BeforeEach
    void init() {
        transactionService = new TransactionService(accountRepository, cardRepository, transactionRepository);
    }
    @Test
    void newTransaction() {
        when(accountRepository.findByUid("account_UID1")).thenReturn(Optional.ofNullable(Account.builder()
                .uid("account_UID1")
                .iban("UA77770000_UID1")
                .balance(1100L)
                .person(Person.builder()
                        .uid("personUID1")
                        .name("TestUser1")
                        .email("test1@email.com")
                        .accounts(List.of())
                        .build())
                .build()));
        when(cardRepository.findByUid("card_UID1")).thenReturn(Optional.ofNullable(Card.builder()
                .uid("card_UID1")
                .pan("7777000000000001")
                .account(Account.builder()
                        .uid("account_UID1")
                        .iban("UA77770000_UID1")
                        .balance(1100L)
                        .person(Person.builder()
                                .uid("personUID1")
                                .name("TestUser1")
                                .email("test1@email.com")
                                .accounts(List.of())
                                .build())
                        .build())
                .expirationDate(Instant.now().plus(364, ChronoUnit.DAYS))
                .pin((short) 1111)
                .cvv((short) 111)
                .status(CardStatus.ACTIVE)
                .person(Person.builder()
                        .uid("personUID1")
                        .name("TestUser1")
                        .email("test1@email.com")
                        .accounts(List.of())
                        .build())
                .build()));

        when(accountRepository.findByUid("account_UID2")).thenReturn(Optional.ofNullable(Account.builder()
                .uid("account_UID2")
                .iban("UA77770000_UID2")
                .balance(0L)
                .person(Person.builder()
                        .uid("personUID2")
                        .name("TestUser2")
                        .email("test2@email.com")
                        .accounts(List.of())
                        .build())
                .build()));
        when(cardRepository.findByUid("card_UID2")).thenReturn(Optional.ofNullable(Card.builder()
                .uid("card_UID2")
                .pan("7777000000000002")
                .account(Account.builder()
                        .uid("account_UID2")
                        .iban("UA77770000_UID2")
                        .balance(0L)
                        .person(Person.builder()
                                .uid("personUID2")
                                .name("TestUser2")
                                .email("test2@email.com")
                                .accounts(List.of())
                                .build())
                        .build())
                .expirationDate(Instant.now().plus(364, ChronoUnit.DAYS))
                .pin((short) 1111)
                .cvv((short) 111)
                .status(CardStatus.ACTIVE)
                .person(Person.builder()
                        .uid("personUID2")
                        .name("TestUser2")
                        .email("test2@email.com")
                        .accounts(List.of())
                        .build())
                .build()));

        when(transactionRepository.save(Mockito.any())).thenAnswer(invocation -> invocation.getArguments()[0]);
        var transactionDto = new TransactionDto(null, "card_UID1", "card_UID2", 100L);
        var result = transactionService.newTransaction(transactionDto);

        assertThat(result.id(), CoreMatchers.notNullValue());
        assertEquals("card_UID1", result.fromCardId());
        assertEquals("card_UID2", result.toCardId());
        assertThat(result.id(), CoreMatchers.notNullValue());
        assertThat(result.id(), hasLength(36));

        Mockito.verify(accountRepository).findByUid("account_UID1");
        Mockito.verify(accountRepository).findByUid("account_UID2");
        Mockito.verify(transactionRepository).save(argumentCaptorTransaction.capture());

        var transaction = argumentCaptorTransaction.getValue();
        assertThat(transaction.getUid(), CoreMatchers.notNullValue());
        assertThat(transaction.getFromCardId(), equalTo("card_UID1"));
        assertThat(transaction.getToCardId(), equalTo("card_UID2"));

        var balance1 = cardRepository.findByUid(result.fromCardId()).orElseThrow().getAccount().getBalance();
        var balance2 = cardRepository.findByUid(result.toCardId()).orElseThrow().getAccount().getBalance();
        log.info("balance1 = " + balance1 + ", balance2 = " + balance2);
//        assertEquals(1000L, balance1);    // как получить новые балансы на счетах ???
//        assertEquals(100L, balance2);
    }
}