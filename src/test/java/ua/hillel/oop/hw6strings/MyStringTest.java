package ua.hillel.oop.hw6strings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static ua.hillel.oop.hw06strings.MyString.*;
import static ua.hillel.oop.hw06strings.MyString.stringReverse;

class MyStringTest {
    @Test
    void shouldFindSymbolOccurance() {
        int count = findSymbolOccurance ("Thkjbа\nfsаbcb\nоyb69а98u0аbhy_joihiugy", '\n');
        assertEquals(2, count);

        count = findSymbolOccurance ("Thkjbа\nfsаbcb\nоyb69а98u0аbhy_joihiugy", 'b');
        assertEquals(5, count);
    }

    @Test
    void shouldFindWordPosition() {
        int pos = findWordPosition("Apollo", "pollo");
        assertEquals(1, pos);

        pos = findWordPosition("Apple", "Ple");
        assertEquals(-1, pos);
    }

    @Test
    void shouldStringReverse() {
        String s1 = "Hello";
        String s2 = stringReverse(s1);
        String s3 = "olleH";
        String s4 = stringReverse(s2);
        assertEquals(s3, s2);
        assertEquals(s1, s4);

        s1 = "Дракоша";
        s2 = stringReverse(stringReverse(s1));
        assertEquals(s1, s2);
    }

    @Test
    void shouldIsPalindrome() {
        boolean res = isPalindrome("ERE");
        assertEquals(true, res);

        res = isPalindrome("Allo");
        assertEquals(false, res);
    }

    @Test
    void shouldCompareWords() {
        String res = compareWords("mushroom", "mushroom");
        assertEquals("mushroom", res);

        res = compareWords("mushrooms", "mushroom");
        assertEquals("mushroom*******", res);

        res = compareWords("mushroom", "mushrooms");
        assertEquals("mushroom*******", res);

        res = compareWords("michcorm", "mushroom");
        assertEquals("m**h*o*m*******", res);
    }
}