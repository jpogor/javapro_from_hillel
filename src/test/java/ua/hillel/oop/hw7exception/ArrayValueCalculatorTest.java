package ua.hillel.oop.hw7exception;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw07exception.ArrayDataException;
import ua.hillel.oop.hw07exception.ArraySizeException;
import ua.hillel.oop.hw07exception.ArrayValueCalculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArrayValueCalculatorTest {

    @Test
    void should_doCalc() {
        String[][] sArr1 = {
                {"1", "2", "3", "4"},
                {"1", "2", "3", "4"},
                {"1", "2", "3", "4"},
                {"1", "2", "3", "5"}
        };
        int total = new ArrayValueCalculator().doCalc(sArr1);
        assertEquals(41, total);
    }

    @Test
    void should_doCalc_ArrayDataException() {
        String[][] sArr2 = {
                {"1", "2", "3", "4"},
                {"1", "2", "aa3", "4"},
                {"1", "2", "3", "4"},
                {"1", "2", "3", "5"}
        };
        var target = new ArrayValueCalculator();
        //ArrayValueCalculator target = new ArrayValueCalculator();
        Exception exception = assertThrows(ArrayDataException.class, () -> target.doCalc(sArr2));
        assertEquals(exception.getMessage(), "Error: Can't parse String to int: Arr[2][3] = aa3");
    }

    @Test
    void should_doCalc_ArraySizeException() {
        String[][] sArr3 = {
                {"1","2","3","4"},
                {"1","2","3","4","5"},
                {"1","2","3","4"},
                {"1","2","3","5"}
        };
        ArrayValueCalculator target = new ArrayValueCalculator();
        Exception exception = assertThrows(ArraySizeException.class, () -> target.doCalc(sArr3));
        assertEquals(exception.getMessage(), "Error: Wrong size of Array. Line 2 must have size = 4. Actual size = 5");
    }
}
