package ua.hillel.oop.hw8collection;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw08collection.MyCollection;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyCollectionTest {

    @Test
    void should_countOccurance() {
        List<String> srcWords = List.of("apple", "orange", "lemon", "grape", "banana", "apricot", "avocado", "broccoli",
                "carrot", "cherry", "garlic", "grape", "melon", "leak", "grape", "kiwi", "mango", "mushroom", "grape", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato");
        int result = MyCollection.countOccurance(srcWords, "grape");
        assertEquals(result, 4);
        result = MyCollection.countOccurance(srcWords, "grapesss");
        assertEquals(result, 0);
    }

    @Test
    void should_toList() {
        String[] arr = {"2","1","3","4","5","6","7","8","9","10","11","12"};
        List<String> target = List.of("1","10","11","12","2","3","4","5","6","7","8","9");
        var result = MyCollection.toList(arr).stream().sorted().toList();
        assertEquals(result, target);
    }

    @Test
    void should_findUnique() {
        List<String> result = MyCollection.findUnique(List.of("7","1","2","10","1","3","4","12","4","9","5","6","7","12","8","9","10","5","11","12","12","4","9","5","6","7"));
        result = result.stream().sorted().toList();
        List<String> target = List.of("1","10","11","12","2","3","4","5","6","7","8","9");
        assertEquals(result, target);
    }

    @Test
    void shouldFindOccurance() {
        List<MyCollection.StringCounter> result = MyCollection.findOccurance(List.of("bird","bird","fox","cat"));
        var target = List.of(
                new MyCollection.StringCounter("bird",2),
                new MyCollection.StringCounter("fox",1),
                new MyCollection.StringCounter("cat",1)
        );
        assertEquals(result, target);

        result.clear();
        result = MyCollection.findOccurance(List.of(
                "bird","bird","fox","cat","bird","fox","cat","bird","fox","cat",
                "bird","bird","fox","cat","bird","fox","cat","bird","fox","cat",
                "bird","bird","fox","cat","bird","fox","cat","bird","fox","cat"
        ));
        target = List.of(
                new MyCollection.StringCounter("bird",12),
                new MyCollection.StringCounter("fox",9),
                new MyCollection.StringCounter("cat",9)
        );
        assertEquals(result, target);
    }
}