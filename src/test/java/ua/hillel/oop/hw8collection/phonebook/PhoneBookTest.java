package ua.hillel.oop.hw8collection.phonebook;

import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw08collection.phonebook.PhoneBook;
import ua.hillel.oop.hw08collection.phonebook.PhoneRecord;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PhoneBookTest {

    @Test
    void should_PhonebookTest() {
        PhoneBook phoneBook = new PhoneBook();

        phoneBook.add(new PhoneRecord("Yuriy", "0635813000"));
        phoneBook.add(new PhoneRecord("Yuriy", "0505813000"));
        phoneBook.add(new PhoneRecord("Yuriy", "0675813000"));
        phoneBook.add(new PhoneRecord("Tanya", "0635813005"));
        phoneBook.add(new PhoneRecord("Tanya", "0675813005"));
        phoneBook.add(new PhoneRecord("Lora", "0635813004"));

        PhoneRecord target = new PhoneRecord("Lora","0635813004");
        PhoneRecord result = phoneBook.find("Lora");
        assertEquals(target, result);

        target = new PhoneRecord("Tanya","0675813005");
        result = phoneBook.find("Tanya");
        assertEquals(target, result);

        target = new PhoneRecord("Yuriy","0675813000");
        result = phoneBook.find("Yuriy");
        assertEquals(target, result);

        result = phoneBook.find("Yurii");
        assertEquals(null, result);

        List<PhoneRecord> targetRec = new ArrayList<PhoneRecord>();
        targetRec.add(new PhoneRecord("Lora","0635813004"));
        List<PhoneRecord> resultList = new ArrayList<PhoneRecord>();
        resultList = phoneBook.findAll("Lora");
        assertEquals(targetRec, resultList);

        targetRec.clear();
        targetRec.add(new PhoneRecord("Tanya","0635813005"));
        targetRec.add(new PhoneRecord("Tanya","0675813005"));
        resultList = phoneBook.findAll("Tanya");
        assertEquals(targetRec, resultList);

        targetRec.clear();
        targetRec.add(new PhoneRecord("Yuriy","0635813000"));
        targetRec.add(new PhoneRecord("Yuriy","0505813000"));
        targetRec.add(new PhoneRecord("Yuriy","0675813000"));
        resultList = phoneBook.findAll("Yuriy");
        assertEquals(targetRec, resultList);

        resultList = phoneBook.findAll("Yurii");
        assertEquals(List.of(), resultList);

    }
}