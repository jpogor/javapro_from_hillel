package ua.hillel.oop.hw9dictionary;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.hillel.oop.hw09dictionary.FileData;
import ua.hillel.oop.hw09dictionary.FileNavigator;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class FileNavigatorTest {

    private FileData fileData1;
    private FileData fileData2;
    private FileData fileData3;
    private final FileNavigator fileNavigator = new FileNavigator();

    @BeforeEach
    void init() {
        fileData1 = new FileData("test_01.txt", 101, "D:\\Work_01");
        fileData2 = new FileData("test_02.txt", 202, "D:\\Work_02");
        fileData3 = new FileData("test_03.txt", 101, "D:\\Work_01");
    }

    @Test
    void shouldAdd() {
        fileNavigator.add("D:\\Work_01", fileData1);
        fileNavigator.add("D:\\Work_02", fileData2);

        var result = fileNavigator.add("D:\\Work_01", fileData3);

        var expected = new HashMap<String, ArrayList<FileData>>();
        var arrFileData1 = new ArrayList<FileData>();
        arrFileData1.add(fileData1);
        arrFileData1.add(fileData3);
        var arrFileData2 = new ArrayList<FileData>();
        arrFileData2.add(fileData2);
        expected.put("D:\\Work_01", arrFileData1);
        expected.put("D:\\Work_02", arrFileData2);

        assertEquals(expected, result);
    }

    @Test
    void shouldFind() {
        fileNavigator.add("D:\\Work_01", fileData1);
        fileNavigator.add("D:\\Work_02", fileData2);
        fileNavigator.add("D:\\Work_01", fileData3);

        var result = fileNavigator.find("D:\\Work_01");

        var expected = new ArrayList<FileData>();
        expected.add(fileData1);
        expected.add(fileData3);

        assertEquals(expected, result);
    }

    @Test
    void shouldFilterBySize() {
        fileNavigator.add("D:\\Work_01", fileData1);
        fileNavigator.add("D:\\Work_02", fileData2);
        fileNavigator.add("D:\\Work_01", fileData3);

        var result = fileNavigator.filterBySize(101);

        var expected = new ArrayList<FileData>();
        expected.add(fileData1);
        expected.add(fileData3);

        assertEquals(expected, result);
    }

    @Test
    void shouldRemove() {
        fileNavigator.add("D:\\Work_01", fileData1);
        fileNavigator.add("D:\\Work_02", fileData2);
        fileNavigator.add("D:\\Work_01", fileData3);

        var result = fileNavigator.remove("D:\\Work_01");

        var expected = new ArrayList<FileData>();
        expected.add(fileData1);
        expected.add(fileData3);

        assertEquals(expected, result);
    }

    @Test
    void shouldSortBySize() {
        fileNavigator.add("D:\\Work_01", fileData1);
        fileNavigator.add("D:\\Work_02", fileData2);
        fileNavigator.add("D:\\Work_01", fileData3);

        var result = fileNavigator.sortBySize();

        var expected = new ArrayList<FileData>();
        expected.add(fileData1);
        expected.add(fileData3);
        expected.add(fileData2);

        assertEquals(expected, result);
    }
}